/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Auxillary.h
 * Author: mike
 *
 * Created on August 12, 2016, 12:32 PM
 */

#ifndef AUXILLARY_H
#define AUXILLARY_H

//#ifdef __cplusplus
//extern "C" {
//#endif
    
 #include "opencv2/opencv.hpp"
#include <vector>
#include <cmath>
using namespace cv;
using namespace std; 

#define sqr(a)  ((a) * (a))
 
void updateDensity(Mat density, Mat img);
void densityMapping(Mat density, Mat img);
void printBackgroundValues(Ptr<BackgroundSubtractorMOG2>  bgR);
char *ret_base_fname(char *arg, char * baseFileName, char * extension);
void DecomposeEssential(Mat Essential, Mat& R1, Mat& R2, Mat& t1, Mat& t2);
Mat_<double> IterativeLinearLSTriangulation(Point3d u, Matx34d P, Point3d u1, Matx34d P1);
Mat createPMatrix(Mat rMat, Mat tvec);
Mat createPMatrix2(Mat rMat, Mat tvec);
void printMatrix(const cv::Mat &M, std::string matrix);
bool CheckCoherentRotation(cv::Mat& R);
bool triangulate(vector<Point2f>imgPntsL, vector<Point2f>imgPntsR, Mat P, Mat P1, Mat intrinsicL, Mat intrinsicR);
bool testRotationMatrices(Mat& E, Mat& R, Mat& R3, Mat& t, Mat& Pret, vector<Point2f>imgPntsL, vector<Point2f>imgPntsR, Mat intrinsicL, Mat intrinsicR);
Mat EssentialFromFundamental(Mat F, Mat IntL, Mat IntR);
Mat createEssentialMatrix(Mat rMat, Mat tvec);
float euclidDistance(Point2f p1, Point2f p2);
int randomInt(int min, int max);
Mat_<double> LinearLSTriangulation(Point3d u, //homogenous image point (u,v,1)
        Matx34d P, //camera 1 matrix
        Point3d u1, //homogenous image point in 2nd camera
        Matx34d P1 //camera 2 matrix
        );
Mat_<double> IterativeLinearLSTriangulation(Point3d u, //homogenous image point (u,v,1)
        Matx34d P, //camera 1 matrix
        Point3d u1, //homogenous image point in 2nd camera
        Matx34d P1 //camera 2 matrix
        );
double scaleRecovery(vector<Point3f> measured, vector<Point3f> actual);




//#ifdef __cplusplus
//}
//#endif

#endif /* AUXILLARY_H */

