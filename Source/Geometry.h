/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Geometry.h
 * Author: Mike
 *
 * Created on July 2, 2016, 8:59 AM
 */

#include "opencv2/opencv.hpp"
//#include "opencv2/nonfree/nonfree.hpp"
#include "Track.h"
#include "Line.h"

#ifndef GEOMETRY_H
#define GEOMETRY_H

class Geometry {
public:
    Geometry();
    Geometry(const Geometry& orig);
    virtual ~Geometry();
    double NearestDistanceSkewLines(Track T1, Track T2);
    cv::Matx31d BestGuessLocation(Track T1, Track T2);
    cv::Matx31d rotateVectorY(double theta, cv::Matx31d inVector);
    cv::Matx31d rotateVectorZ(double theta, cv::Matx31d inVector);
    cv::Matx31d rotateVectorX(double theta, cv::Matx31d inVector);
    double ConvertDegreeToRadian(double degree);
private:

};

#endif /* GEOMETRY_H */

