/* 
 * File:   trackerAnalysis.h
 * Author: mcaprio2
 *
 * Created on July 15, 2014, 1:39 PM
 */
#include "tracker.h"

#ifndef TRACKERANALYSIS_H
#define	TRACKERANALYSIS_H

class trackerAnalysis : public tracker {
public:
  trackerAnalysis();
  trackerAnalysis(const trackerAnalysis& orig);
  virtual ~trackerAnalysis();
  void image2Update(Mat *i);
  void updateOutputImage(int cCount);
  void forward(int trackNum, int editPosition, int *jump);
  virtual void writeATrack(int index, int mAvr, double fps, char * baseFileName, char *trackname);
    //virtual void followATrack(int index);
private:
  Mat *im_out;
};

#endif	/* TRACKERANALYSIS_H */

