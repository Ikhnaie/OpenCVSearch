/* 
 * File:   PotentialBug.cpp
 * Author: mcaprio2
 * 
 * Created on June 20, 2014, 12:57 PM
 */

#include "PotentialBug.h"

PotentialBug::PotentialBug() {
  tracked = false;
}

void PotentialBug::SetTracked(bool tracked) {
  this->tracked = tracked;
}

bool PotentialBug::IsTracked() const {
  return tracked;
}

PotentialBug::PotentialBug(const PotentialBug& orig) {
  location = orig.location;
  lastFrame = orig.lastFrame;
  lineColor = orig.lineColor;
  index = orig.index;
  tracked = orig.tracked;
}

PotentialBug::PotentialBug(Point2f pnt, unsigned int frameCount) {
  location = pnt;
  lastFrame = frameCount;
  tracked = false;
}

PotentialBug::~PotentialBug() {
}

PotentialBug& PotentialBug::operator=(const PotentialBug& a) {
  location = a.location;
  lastFrame = a.lastFrame;
  lineColor = a.lineColor;
  index = a.index;
  tracked = a.tracked;
};

