/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Line.h
 * Author: mcaprio3
 *
 * Created on July 1, 2016, 10:56 AM
 */
#include "opencv2/opencv.hpp"
//#include </usr/local/include/opencv2/opencv.hpp>
//#include "opencv2/nonfree/nonfree.hpp"

#ifndef LINE_H
#define LINE_H

class Line {
public:
    Line();
    Line(const Line& orig);
    virtual ~Line();
    
    cv::Matx31d P0, P1;
private:

};

#endif /* LINE_H */

