/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CommonParameters.cpp
 * Author: Mike
 * 
 * Created on September 28, 2016, 7:55 PM
 */

#include "CommonParameters.h"
#include "GlobalDefinitions.h"
using namespace cv;
using namespace std;

CommonParameters::CommonParameters() {
 #ifdef USECUDA
    bg = cv::cuda::createBackgroundSubtractorMOG2();
    bg2 = cv::cuda::createBackgroundSubtractorMOG2();
#else 
    bg = createBackgroundSubtractorMOG2();
    bg2 = createBackgroundSubtractorMOG2();
#endif
}

CommonParameters::CommonParameters(const CommonParameters& orig) {
    Gaussian = orig.Gaussian;
    AdaptiveBackgroundModeling - orig.AdaptiveBackgroundModeling;
    TrackerMinSize = orig.TrackerMinSize;
    GaussianBlurSize = orig.GaussianBlurSize;
    Erode1 = orig.Erode1;
    Erode2 = orig.Erode2;
    Dilate1 = orig.Dilate1;
    KeyPointCircleSize = orig.KeyPointCircleSize;
    TrackCircleSize = orig.TrackCircleSize;
    KalmanCircleSize = orig.KalmanCircleSize;
    bg = orig.bg;
    params = orig.params;
    
}

CommonParameters::~CommonParameters() {
}

void CommonParameters::SetGaussianBlurSize(int GaussianBlurSize) {
    this->GaussianBlurSize = GaussianBlurSize;
}

int CommonParameters::GetGaussianBlurSize() const {
    return GaussianBlurSize;
}

void CommonParameters::SetTrackerMinSize(int TrackerMinSize) {
    this->TrackerMinSize = TrackerMinSize;
}

int CommonParameters::GetTrackerMinSize() const {
    return TrackerMinSize;
}

void CommonParameters::SetAdaptiveBackgroundModeling(bool AdaptiveBackgroundModeling) {
    this->AdaptiveBackgroundModeling = AdaptiveBackgroundModeling;
}

bool CommonParameters::IsAdaptiveBackgroundModeling() const {
    return AdaptiveBackgroundModeling;
}

void CommonParameters::SetGaussian(bool Gaussian) {
    this->Gaussian = Gaussian;
}

bool CommonParameters::IsGaussian() const {
    return Gaussian;
}

void CommonParameters::SetParams(SimpleBlobDetector::Params params) {
    this->params = params;
}

SimpleBlobDetector::Params CommonParameters::GetParams() const {
    return params;
}


void CommonParameters::SetDilate1(int Dilate1) {
    this->Dilate1 = Dilate1;
}

int CommonParameters::GetDilate1() const {
    return Dilate1;
}

void CommonParameters::SetErode2(int Erode2) {
    this->Erode2 = Erode2;
}

int CommonParameters::GetErode2() const {
    return Erode2;
}

void CommonParameters::SetErode1(int Erode1) {
    this->Erode1 = Erode1;
}

int CommonParameters::GetErode1() const {
    return Erode1;
}

void CommonParameters::SetKalmanCircleSize(int KalmanCircleSize) {
    this->KalmanCircleSize = KalmanCircleSize;
}

int CommonParameters::GetKalmanCircleSize() const {
    return KalmanCircleSize;
}

void CommonParameters::SetTrackCircleSize(int TrackCircleSize) {
    this->TrackCircleSize = TrackCircleSize;
}

int CommonParameters::GetTrackCircleSize() const {
    return TrackCircleSize;
}

void CommonParameters::SetKeyPointCircleSize(int KeyPointCircleSize) {
    this->KeyPointCircleSize = KeyPointCircleSize;
}

int CommonParameters::GetKeyPointCircleSize() const {
    return KeyPointCircleSize;
}

