/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Geometry.cpp
 * Author: Mike
 * 
 * Created on July 2, 2016, 8:59 AM
 */
#include "opencv2/opencv.hpp"
//#include "opencv2/nonfree/nonfree.hpp"

#include "Geometry.h"

#define SMALL_NUM   0.00000001 // anything that avoids division overflow
#define dot(u,v) ((u(0,0) * v(0,0) + u(1,0) * v(1,0) + u(2,0) * v(2,0)))
#define norm(v)    sqrt(dot(v,v))  // norm = length of  vector
#define d(u,v)     norm(u-v)        // distance = norm of difference
#define abs(x)     ((x) >= 0 ? (x) : -(x))   //  absolute value

Geometry::Geometry() {
}

Geometry::Geometry(const Geometry& orig) {
}

Geometry::~Geometry() {
}

double Geometry::ConvertDegreeToRadian(double degree){
    return 3.14159/180.0 * degree;
}


cv::Matx31d Geometry::rotateVectorY(double theta, cv::Matx31d inVector){
    double radian = theta;
    cv::Matx33d yrot(cos(radian), 0, sin(radian),  0.0, 1.0, 0.0, -sin(radian), 0, cos(radian));
    return yrot * inVector;
}

cv::Matx31d Geometry::rotateVectorZ(double theta, cv::Matx31d inVector){
    double radian = theta;
    cv::Matx33d zrot(cos(radian), -sin(radian), 0,  sin(radian), cos(radian), 0, 0, 0, 1);
    return zrot * inVector;
}

cv::Matx31d Geometry::rotateVectorX(double theta, cv::Matx31d inVector){
    double radian = theta;
    cv::Matx33d xrot(1.0,0.0,0.0, 0.0, cos(radian), -sin(radian),  0.0, sin(radian), cos(radian));
    return xrot * inVector;
}


cv::Matx31d Geometry::BestGuessLocation(Track T1, Track T2){
    cv::Matx31d   u = T1.v;
    cv::Matx31d   v = T2.v;
    cv::Matx31d   w = T1.P0 - T2.P0;
    float    a = dot(u,u);         // always >= 0
    float    b = dot(u,v);
    float    c = dot(v,v);         // always >= 0
    float    d = dot(u,w);
    float    e = dot(v,w);
    float    D = a*c - b*b;        // always >= 0
    float    sc, tc;

    // compute the line parameters of the two closest points
    if (D < SMALL_NUM) {          // the lines are almost parallel
        sc = 0.0;
        tc = (b>c ? d/b : e/c);    // use the largest denominator
    }
    else {
        sc = (b*e - c*d) / D;
        tc = (a*e - b*d) / D;
    }

    // get the location of the two closest points
    cv::Matx31d  L1p = sc * u + T1.P0;
    cv::Matx31d  L2p = tc * v + T2.P0;
         

    return (L1p + L2p) * 0.5;   // return the best guess
}

double Geometry::NearestDistanceSkewLines(Track T1, Track T2){
    cv::Matx31d   u = T1.v;
    cv::Matx31d   v = T2.v;
    cv::Matx31d   w = T1.P0 - T2.P0;
    double    a = dot(u,u);         // always >= 0
    double    b = dot(u,v);
    double    c = dot(v,v);         // always >= 0
    double    d = dot(u,w);
    double    e = dot(v,w);
    double    D = a*c - b*b;        // always >= 0
    double    sc, tc;

    // compute the line parameters of the two closest points
    if (D < SMALL_NUM) {          // the lines are almost parallel
        sc = 0.0;
        tc = (b>c ? d/b : e/c);    // use the largest denominator
    }
    else {
        sc = (b*e - c*d) / D;
        tc = (a*e - b*d) / D;
    }

    // get the difference of the two closest points
    cv::Matx31d   dP = w + (sc * u) - (tc * v);  // =  L1(sc) - L2(tc)

    return norm(dP);   // return the closest distance
}
//===================================================================
  

