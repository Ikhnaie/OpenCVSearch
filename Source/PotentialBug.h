/* 
 * File:   PotentialBug.h
 * Author: mcaprio2
 *
 * Created on June 20, 2014, 12:57 PM
 */
#include <stdlib.h>
#include <stdio.h>
#include "opencv2/opencv.hpp"
//#include </usr/local/include/opencv2/opencv.hpp>
//#include "opencv2/nonfree/nonfree.hpp"
#include <cmath>
using namespace cv;

#ifndef POTENTIALBUG_H
#define	POTENTIALBUG_H

class PotentialBug {
public:
  PotentialBug();
  PotentialBug(Point2f point, unsigned int frameCount);
  PotentialBug(const PotentialBug& orig);
  virtual ~PotentialBug();
  PotentialBug& operator=(const PotentialBug& a);

  void SetLastFrame(unsigned int lastFrame) {
    this->lastFrame = lastFrame;
  }

  unsigned int GetLastFrame() const {
    return lastFrame;
  }

  void SetLocation(Point2f location) {
    this->location = location;
  }

  Point2f GetLocation() const {
    return location;
  }

  void SetLineColor(Scalar lineColor) {
    this->lineColor = lineColor;
  }

  Scalar GetLineColor() const {
    return lineColor;
  }

  void SetIndex(unsigned int index) {
      this->index = index;
  }

  unsigned int GetIndex() const {
      return index;
  }
  void SetTracked(bool tracked);
  bool IsTracked() const;
private:
  Point2f location;
  unsigned int lastFrame, index;
  Scalar lineColor;
  bool tracked;
  

};

#endif	/* POTENTIALBUG_H */

