/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Deinterlace.cpp
 * Author: Mike
 * 
 * Created on May 31, 2016, 6:21 PM
 */

#include "Deinterlace.h"

Deinterlace::Deinterlace( ) {
}

Deinterlace::Deinterlace(const Deinterlace& orig) {
}

Deinterlace::~Deinterlace() {
}

void Deinterlace::deinterlaceImage(Mat inImage, Mat outImage, bool even){

    if (even){
        for (int j = 0; j < inImage.rows; j +=2){
          inImage.row(j).copyTo(outImage.row(j));
          inImage.row(j).copyTo(outImage.row(j+1));
        }
    } else {
        for (int j = 1; j <= inImage.rows; j +=2){
          inImage.row(j).copyTo(outImage.row(j-1));
          inImage.row(j).copyTo(outImage.row(j));
        }        
    }    
}

