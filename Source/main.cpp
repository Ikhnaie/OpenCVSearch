
#include <stdlib.h>
#include <stdio.h>
#include "opencv2/opencv.hpp"
#include "opencv2/core/ocl.hpp"
#include "opencv2/features2d/features2d.hpp"
//#include "opencv2/cudabgsegm.hpp"
//#include "opencv2/cudalegacy.hpp"
#include <vector>
#include <cmath>
#include <pthread.h>
#include <unistd.h>
#include <omp.h>
//#include<random>
//#include "cvsba.h"
//#include "sba.h"
//#include "compiler.h"

#include "LineDesc.h"
#include "PotentialBug.h"
#include "tracker.h"
//#include "analyzeFrame.h"
#include "Deinterlace.h"
#include "Track.h"
#include "Line.h"
#include "Geometry.h"
#include "Auxillary.h"
#include "CommonParameters.h"
#include "GlobalDefinitions.h"

using namespace cv;
using namespace std;
using namespace cv::cuda;

#define sqr(a)  ((a) * (a))

template <typename T>
static float distancePointLine(const cv::Point_<T> point, const cv::Vec<T, 3>& line) {
  //Line is given as a*x + b*y + c = 0  Needed by drawEpipolarLines
  return fabsf(line(0) * point.x + line(1) * point.y + line(2))
          / std::sqrt(line(0) * line(0) + line(1) * line(1));
}



int movingAverage;
//Mat *im_out2;
Mat img3L, img3R, img3Lc, img3Rc; //img2MappedL, img2MappedR;
vector<UMat> imgMapped;
double fps;
bool vflag = false;
bool cleanScreen = false;
char baseFileName[FILENAMESIZE], baseFileNameL[FILENAMESIZE], baseFileNameR[FILENAMESIZE];
int track2Follow = -1;
int track2FollowR = -1;
int t2fl = -1;
int t2fr = -1;
int trackLength = 0;
int trackLengthR = 0;
unsigned int frameCount = 0, frameCountR = 0, frameCountL = 0;
vector<vector<KeyPoint> > keypointsBlob;
KalmanFilter KF(4, 2, 0);
KalmanFilter KFR(4, 2, 0);
Point predictPt, predictPtR;
Mat fm;
CommonParameters CPs;

void redrawKeypoints(vector<KeyPoint> &keypointsBlob, Mat img3) {
  Point2f center;

  if (keypointsBlob.size() > 0) {
    for (int j1 = 0; j1 < keypointsBlob.size(); j1++) {
      center.x = keypointsBlob[j1].pt.x;
      center.y = keypointsBlob[j1].pt.y;
      circle(img3, center, CPs.GetKeyPointCircleSize(), Scalar(0, 0, 255), 2, 8, 0);
    }
  }
}

int main(int argc, char *argv[]) {
  Mat imgBackgroundModel, im_out, subtracted_frame_shrunk;
  Mat backL, backR;
  int skipFrames = 0;
  int initialSkip = 0;
  int nCores = 1;
  Point2f prevCenter;
  char jpegFileName[FILENAMESIZE];
  char outputFileName[FILENAMESIZE], outputFileNameR[FILENAMESIZE];
  char subFileName[FILENAMESIZE];
  char reFileName[FILENAMESIZE];
  char fsFileName[FILENAMESIZE];
  bool reWrite = false;
  int64 e1, e2;
  double execTime;
  bool saveBackgroundModel = false;
  Deinterlace deinterlacer;
  Mat prediction;
  Point predictPt1;
  bool useKalmanPredict = false;
  bool useCUDA = false;
  bool useOPENCL = false;
  char extension[FILENAMESIZE];
  int rightAdjustFrame = 0;
  bool timeProgram = false;
  int timeCount = 0;
  vector<Mat> imagesLeft;
  vector<Mat>imagesRight;
  int imageLeftP, imageRightP;
  int maxUndo = 5;
  int cameraSet = 1;

  char *fname = NULL;
  int index;
  int c;
  opterr = 0;
  while ((c = getopt(argc, argv, "CNsvrkc:a:j:f:i:m:")) != -1)
    switch (c) {
      case 'r':
        reWrite = true;
        break;
      case 'v':
        vflag = true;
        break;
      case 'j':
        nCores = atoi(optarg);
        break;
      case 'i':
        initialSkip = atoi(optarg);
        break;
      case 'm':
        movingAverage = atoi(optarg);
        break;
      case 'f':
        fname = optarg;
        break;
      case 's':
        saveBackgroundModel = true;
        break;
      case 'k':
        useKalmanPredict = true;
        break;
      case 'a':
        rightAdjustFrame = atoi(optarg);
        break;
      case 'N':
        useCUDA = true;
        useCUDA = false; //I've removed CUDA code, use openCL instead
        break;
      case 'C':
        useOPENCL = true;
        break;
      case 'c':
        cameraSet = atoi(optarg);
        break;
      case '?':
        if (optopt == 'c')
          fprintf(stderr, "Option -%c requires an argument.\n", optopt);
        else if (isprint(optopt))
          fprintf(stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf(stderr,
                "Unknown option character `\\x%x'.\n",
                optopt);
        return 1;
      default:
        abort();
    }
  Mat intrinsic_matrixL = Mat::eye(3, 3, CV_64F);
  Mat intrinsic_matrixR = Mat::eye(3, 3, CV_64F);

  Mat distortion_coeffsL = Mat::zeros(8, 1, CV_64F);
  Mat distortion_coeffsR = Mat::zeros(8, 1, CV_64F);

  //ret_base_fname(fname, baseFileName, extension);
  char* fnameL;
  char* fnameR;
  char *pFile;
  char baseFileNameR[FILENAMESIZE], baseFileNameL[FILENAMESIZE];
  char paramFileName[FILENAMESIZE];
  ret_base_fname(fname, baseFileName, extension);
  if (strlen(baseFileName) < FILENAMESIZE) {
    strcpy(baseFileNameL, baseFileName);
    strcpy(baseFileNameL, baseFileName);
    strcpy(baseFileNameR, baseFileName);
    strcpy(paramFileName, baseFileName);
    fnameL = strcat(strcat(baseFileNameL, "L"), extension);
    fnameR = strcat(strcat(baseFileNameR, "R"), extension);
    pFile = strcat(strcat(paramFileName, "-parameters"), ".xml");
  } else {
    printf("Filename exceeds maxsize\n");
    exit(-1);
  }
  cout << fnameL << "  " << fnameR << endl;


  FileStorage fs;
  sprintf(fsFileName, "%s%d.xml", "Camera", cameraSet);
  fs.open(fsFileName, FileStorage::READ);
  if (!fs.isOpened()) {
    cerr << "Failed to open " << "Camera" << cameraSet << ".xml" << endl;
    return 1;
  }
  fs["Intrinsics"] >> intrinsic_matrixL;
  fs["DistortionCoefs"] >> distortion_coeffsL;

  fs.release();

  sprintf(fsFileName, "%s%d.xml", "Camera", cameraSet + 1);
  fs.open(fsFileName, FileStorage::READ);
  if (!fs.isOpened()) {
    cerr << "Failed to open " << "Camera" << cameraSet+1 << ".xml" << endl;
    return 1;
  }
  fs["Intrinsics"] >> intrinsic_matrixR;
  fs["DistortionCoefs"] >> distortion_coeffsR;

  fs.release();
  Mat RotL, RotR, transL, transR;

  char baseFileNameX[FILENAMESIZE];
  vector<Point2f> imagePoints2L, imagePoints2R;
  if (strlen(baseFileName) < FILENAMESIZE) {
    strcpy(baseFileNameX, baseFileName);
    strcat(baseFileNameX, ".xml");
  } else {
    printf("Max file name size exceeded\n");
  }
  fs.open(baseFileNameX, FileStorage::READ);
  if (!fs.isOpened()) {
    cerr << "Failed to open " << baseFileNameX << endl;
    return 1;
  }
  fs["Left"] >> imagePoints2L;
  fs["Right"] >> imagePoints2R;


  fs ["RotMatrixL"] >> RotL;
  fs ["TranlationL"] >> transL;
  fs ["RotMatrixR"] >> RotR;
  fs ["TranlationR"] >> transR;
  fs ["Fundamental"] >> fm;
  fs.release();



  Ptr<cv::BackgroundSubtractorMOG2> bgL = cv::createBackgroundSubtractorMOG2();
  Ptr<cv::BackgroundSubtractorMOG2> bgR = cv::createBackgroundSubtractorMOG2();

  fs.open(pFile, FileStorage::READ);
  if (!fs.isOpened()) {
    cerr << "Failed to open " << pFile << endl;
    return 1;
  }
  FileNode n = fs["bgParameters"];
  double bgLearnRate = (double) n["LearnRate"];
  //0 means that the background model is not updated at all, 
  //1 means that the background model is completely reinitialized from the last frame. 
  bgR->setNMixtures((int) n["nmixtures"]);
  //0 is defined as false
  bgR->setDetectShadows((int) n["detectShadows"] != 0);
  bgR->setBackgroundRatio((double) n["backgroundRatio"]);
  bgR->setVarThresholdGen((double) n["varThresholdGen"]);
  bgR->setComplexityReductionThreshold((double) n["fCT"]);
  bgR->setHistory((int) n["History"]);

  bgL->setNMixtures((int) n["nmixtures"]);
  bgL->setDetectShadows((int) n["detectShadows"] != 0);
  bgL->setBackgroundRatio((double) n["backgroundRatio"]);
  bgL->setVarThresholdGen((double) n["varThresholdGen"]);
  bgL->setComplexityReductionThreshold((double) n["fCT"]);
  bgL->setHistory((int) n["History"]);

  n = fs["SimpleParams"];
  CPs.params.read(n);

  n = fs["GeneralParameters"];
  CPs.SetErode1((int) n["Erode1"]);
  CPs.SetDilate1((int) n["Dilate1"]);
  CPs.SetErode2((int) n["Erode2"]);
  CPs.SetTrackerMinSize((int) n["TrackMinDistance"]);
  CPs.SetAdaptiveBackgroundModeling((int) n["AdaptiveBackgroundModeling"] != 0);
  CPs.SetGaussian((int) n["Gaussian"] != 0);
  CPs.SetGaussianBlurSize((int) n["GaussianBlur"]);
  CPs.SetKeyPointCircleSize((int) n["KeypointCircle"]);
  CPs.SetTrackCircleSize((int) n["TrackCircle"]);
  CPs.SetKalmanCircleSize((int) n["KalmanCircle"]);

  fs.release();
  Ptr<SimpleBlobDetector> detector = SimpleBlobDetector::create(CPs.params);
  //bgR = CPs.bg;

  Mat Psba = createPMatrix(RotL, transL);
  Mat P1sba = createPMatrix(RotR, transR);

  printf("Lefthand\nfx %f, fy %f \n", intrinsic_matrixL.at<double>(0, 0), intrinsic_matrixL.at<double>(1, 1));
  printf("cx %f, cy %f\n", intrinsic_matrixL.at<double>(0, 2), intrinsic_matrixL.at<double>(1, 2));
  printf("Righthandhand \nfx %f, fy %f \n", intrinsic_matrixR.at<double>(0, 0), intrinsic_matrixR.at<double>(1, 1));
  printf("cx %f, cy %f\n", intrinsic_matrixR.at<double>(0, 2), intrinsic_matrixR.at<double>(1, 2));
  setNumThreads(nCores);
  printf("Number threads: %d\n", getNumThreads());
  printf("Number of CPUs: %d\n", getNumberOfCPUs());


  imgMapped.resize(2);
  keypointsBlob.resize(2);

  vector<UMat> subtractedFrames;
  subtractedFrames.resize(2);


  vector<UMat> mapx;
  mapx.resize(2);

  vector<UMat> mapy;
  mapy.resize(2);

  VideoCapture captureL(fnameL);
  VideoCapture captureR(fnameR);
  if (!captureL.isOpened()) {// if not success, exit program
    printf("Cannot open the video file %s", fnameL);
    return -1;
  }

  if (!captureR.isOpened()) {// if not success, exit program
    puts("Cannot open the right camera video file");
    return -1;
  }

  double msecPerFrameL = 1000.0 / captureL.get(CV_CAP_PROP_FPS);
  double msecPerFrameR = 1000.0 / captureR.get(CV_CAP_PROP_FPS);


  if (initialSkip > abs(rightAdjustFrame)) {
    captureL.set(CV_CAP_PROP_POS_FRAMES, frameCountL = initialSkip);
    captureR.set(CV_CAP_PROP_POS_FRAMES, frameCountR = initialSkip + rightAdjustFrame);
  } else {
    if (rightAdjustFrame < 0) {
      captureL.set(CV_CAP_PROP_POS_FRAMES, frameCountL = initialSkip - rightAdjustFrame);
      captureR.set(CV_CAP_PROP_POS_FRAMES, frameCountR = initialSkip);
    } else {
      captureL.set(CV_CAP_PROP_POS_FRAMES, frameCountL = initialSkip);
      captureR.set(CV_CAP_PROP_POS_FRAMES, frameCountR = initialSkip + rightAdjustFrame);
    }
  }



  frameCount = 0;
  int frameBaseL = frameCountL;
  int frameBaseR = frameCountR;


  bool bSuccess = captureL.read(imgMapped[0]); // read a new frame from video
  if (!bSuccess) {//if not success, break loop
    puts("Cannot read the frame from video file");
    exit(-1);
  }

  bSuccess = captureR.read(imgMapped[1]); // read a new frame from video
  if (!bSuccess) {//if not success, break loop
    puts("Cannot read the frame from video file");
    exit(-1);
  }

  //Mat densityL = Mat_<unsigned int> (imgMapped[0].size());
  //Mat densityR = Mat_<unsigned int> (imgMapped[1].size());
  //Mat densityMapL = Mat_<uchar> (imgMapped[0].size());
  //Mat densityMapR = Mat_<uchar> (imgMapped[1].size());
  //densityL.setTo(0);
  //densityR.setTo(0);
  imagesLeft.resize(maxUndo);
  imagesRight.resize(maxUndo);

  // create a window
  namedWindow("RightCamera", CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);
  moveWindow("RightCamera", 975, 25);

  namedWindow("LeftCamera", CV_WINDOW_NORMAL | CV_WINDOW_KEEPRATIO | CV_GUI_EXPANDED);
  moveWindow("LeftCamera", 25, 25);


  resizeWindow("LeftCamera", imgMapped[0].cols / 2, imgMapped[0].rows / 2);
  resizeWindow("RightCamera", imgMapped[1].cols / 2, imgMapped[1].rows / 2);

  initUndistortRectifyMap(intrinsic_matrixL, distortion_coeffsL, Mat(),
          getOptimalNewCameraMatrix(intrinsic_matrixL, distortion_coeffsL, imgMapped[0].size(), 1, imgMapped[0].size(), 0),
          imgMapped[0].size(), CV_32FC1, mapx[0], mapy[0]);

  initUndistortRectifyMap(intrinsic_matrixR, distortion_coeffsR, Mat(),
          getOptimalNewCameraMatrix(intrinsic_matrixR, distortion_coeffsR, imgMapped[1].size(), 1, imgMapped[1].size(), 0),
          imgMapped[1].size(), CV_32FC1, mapx[1], mapy[1]);


  //Setup Kalman filter
  //This should be moved to become a property of each track
  KF.transitionMatrix = (Mat_<float>(4, 4) << 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1);
  Mat_<float> measurement(2, 1);
  measurement.setTo(Scalar(0));

  KFR.transitionMatrix = (Mat_<float>(4, 4) << 1, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1);
  Mat_<float> measurementR(2, 1);
  measurementR.setTo(Scalar(0));

  if (REMAPPING) {
    UMat imgMapped0;
    cv::remap(imgMapped[0], imgMapped0, mapx[0], mapy[0], INTER_LINEAR);
    imgMapped0.copyTo(imgMapped[0]);
    cv::remap(imgMapped[1], imgMapped0, mapx[1], mapy[1], INTER_LINEAR);
    imgMapped0.copyTo(imgMapped[1]);
  }

  if (CPs.IsGaussian()) {
    Mat imgblur;
    GaussianBlur(imgMapped[0], imgblur, Size(CPs.GetGaussianBlurSize(), CPs.GetGaussianBlurSize()), 0, 0);
    imgblur.copyTo(imgMapped[0]);
    GaussianBlur(imgMapped[1], imgblur, Size(CPs.GetGaussianBlurSize(), CPs.GetGaussianBlurSize()), 0, 0);
    imgblur.copyTo(imgMapped[1]);
  }


  imgMapped[0].copyTo(imgBackgroundModel);
  frameCount = 0;

  imgMapped[0].copyTo(imagesLeft[0]);
  imgMapped[1].copyTo(imagesRight[0]);

  Mat img4L(imgMapped[0].size(), CV_8UC3);
  img4L.setTo(Scalar(0, 0, 0));
  tracker trackerList(&img4L);
  trackerList.setCPs1(CPs);
  trackerList.setID(0);

  Mat img4R(imgMapped[1].size(), CV_8UC3);
  img4R.setTo(Scalar(0, 0, 0));
  tracker trackerListR(&img4R);
  trackerListR.setCPs1(CPs);
  trackerListR.setID(1);

  sprintf(outputFileName, "%s-Analysis.avi", baseFileName);
  sprintf(subFileName, "%s-Subtracted.avi", baseFileName);
  sprintf(reFileName, "%s-re.avi", baseFileName);
  sprintf(outputFileNameR, "%s-AnalysisR.avi", baseFileName);

  fps = captureL.get(CV_CAP_PROP_FPS);
  VideoWriter writer(outputFileName, CV_FOURCC('M', 'P', '4', '2'), captureL.get(CV_CAP_PROP_FPS), imgMapped[0].size(), true); //initialize the VideoWriter object 
  VideoWriter subWriter(subFileName, CV_FOURCC('M', 'P', '4', '2'), captureL.get(CV_CAP_PROP_FPS), imgMapped[0].size(), false); //initialize the VideoWriter object    
  VideoWriter reWriter(reFileName, CV_FOURCC('M', 'P', '4', '2'), captureL.get(CV_CAP_PROP_FPS), imgMapped[0].size(), true); //initialize the VideoWriter object 
  VideoWriter writerR(outputFileNameR, CV_FOURCC('M', 'P', '4', '2'), captureL.get(CV_CAP_PROP_FPS), imgMapped[0].size(), true); //initialize the VideoWriter object 

  printf("Left video reported %6.3f FPS \n", captureL.get(CV_CAP_PROP_FPS));
  printf("Right video reported %6.3f FPS \n", captureR.get(CV_CAP_PROP_FPS));


#define USEOPENMP          
  if (!cv::ocl::haveOpenCL()) {
    cout << "OpenCL is not available..." << endl;
  }

  cv::ocl::Context context;
  if (!context.create(cv::ocl::Device::TYPE_GPU)) {
    cout << "Failed creating the context..." << endl;
  }

  cout << context.ndevices() << " GPU devices are detected." << endl; //This bit provides an overview of the OpenCL devices you have in your computer
  for (int i = 0; i < context.ndevices(); i++) {
    cv::ocl::Device device = context.device(i);
    cout << "name:              " << device.name() << endl;
    cout << "available:         " << device.available() << endl;
    cout << "imageSupport:      " << device.imageSupport() << endl;
    cout << "OpenCL_C_Version:  " << device.OpenCL_C_Version() << endl;
    cout << endl;
  }

  //cv::ocl::Device(context.device(0)); //Here is where you change which GPU to use (e.g. 0 or 1)

  if (useOPENCL)
    cv::ocl::setUseOpenCL(true);
  else
    cv::ocl::setUseOpenCL(false);

  vector<Ptr < cv::BackgroundSubtractorMOG2>> bgObjects;

  bgObjects.push_back(bgL);
  bgObjects.push_back(bgR);

  //vector<UMat> fore;
  //fore.resize(2);

  vector<Mat> backg;
  backg.resize(2);



  bool breakLoop = false;
  bool even = true;
  bool pauser = false;

  imshow("RightCamera", imgMapped[1]);
  imshow("LeftCamera", imgMapped[0]);
  waitKey(10);

  Mat erodeElement = getStructuringElement(MORPH_RECT, Size(3, 3));
  Mat dilateElement = getStructuringElement(MORPH_RECT, Size(5, 5));

  e1 = getTickCount();



  for (;;) {
    if (breakLoop)
      break;
    if (even) {
      if (!pauser) {
        Mat imgIn; //reading directly to imgMapped[] led to skipped images
        //bSuccess = captureL.read(imgMapped[0]);
        bSuccess = captureL.grab();
        bSuccess = captureR.grab();
        bSuccess = captureL.retrieve(imgIn);
        if (!bSuccess) {
          printf("Failed to read from left video on frame %d\n", frameCount);
          exit(0);
        }
        imgIn.copyTo(imgMapped[0]);
        //img2Lorg = img2L.clone();
        //deinterlacer.deinterlaceImage(img2Lorg, img2L, even);

        //bSuccess = captureR.read(imgMapped[1]);
        bSuccess = captureR.retrieve(imgIn);
        if (!bSuccess) {
          printf("Failed to read from right video on frame %d\n", frameCount);
          exit(0);
        }
        imgIn.copyTo(imgMapped[1]);
        //img2Rorg = img2R.clone();
        //deinterlacer.deinterlaceImage(img2Rorg, img2R, even);


        if (!bSuccess) break;
        frameCount++;
        frameCountR++;
        even = !even;
      } else {
        pauser = false;
      }
    } else {
      // deinterlacer.deinterlaceImage(img2Lorg, img2L, even);
      // deinterlacer.deinterlaceImage(img2Rorg, img2R, even);
      // frameCount++;
      even = !even;
      continue;
    }
    even = !even;


    if (REMAPPING) {
      UMat imgUnmapped;
      //#pragma omp parallel for //num_threads(1)         
      for (int ID = 0; ID < 2; ID++) {
        imgMapped[ID].copyTo(imgUnmapped);
        cv::remap(imgUnmapped, imgMapped[ID], mapx[ID], mapy[ID], INTER_LINEAR);
      }
    }


    if (CPs.IsGaussian()) {
      UMat imgblur;
      GaussianBlur(imgMapped[0], imgblur, Size(CPs.GetGaussianBlurSize(), CPs.GetGaussianBlurSize()), 0, 0);
      imgblur.copyTo(imgMapped[0]);
      GaussianBlur(imgMapped[1], imgblur, Size(CPs.GetGaussianBlurSize(), CPs.GetGaussianBlurSize()), 0, 0);
      imgblur.copyTo(imgMapped[1]);
    }

    //#pragma omp parallel for //num_threads(2)  //Might help marginally   
    for (int ID = 0; ID < 2; ID++) {
      bgObjects[ID]->apply(imgMapped[ID], subtractedFrames[ID], bgLearnRate);
      bgObjects[ID]->getBackgroundImage(backg[ID]);
    }


    for (int j = 0; j < CPs.GetErode1(); j++) {
      erode(subtractedFrames[0], subtractedFrames[0], erodeElement);
      erode(subtractedFrames[1], subtractedFrames[1], erodeElement);
    }

    for (int j = 0; j < CPs.GetDilate1(); j++) {
      dilate(subtractedFrames[0], subtractedFrames[0], dilateElement);
      dilate(subtractedFrames[1], subtractedFrames[1], dilateElement);
    }

    for (int j = 0; j < CPs.GetErode2(); j++) {
      erode(subtractedFrames[0], subtractedFrames[0], erodeElement);
      erode(subtractedFrames[1], subtractedFrames[1], erodeElement);
    }

#pragma omp parallel for num_threads(2) //This seemed to help more than any other
    for (int ID = 0; ID < 2; ID++) {
      detector->detect(subtractedFrames[ID], keypointsBlob[ID]);
    }


    Point2f center;
    if (keypointsBlob[0].size() > 0) {
#pragma omp parallel for       //verify that this is not a problem
      for (int j1 = 0; j1 < keypointsBlob[0].size(); j1++) {
        center.x = keypointsBlob[0][j1].pt.x;
        center.y = keypointsBlob[0][j1].pt.y;
        PotentialBug *pb = new PotentialBug(center, frameCount);
        trackerList.addPotentialBug(pb);
      }
    }

    if (keypointsBlob[1].size() > 0) {
#pragma omp parallel for       
      for (int j1 = 0; j1 < keypointsBlob[1].size(); j1++) {
        center.x = keypointsBlob[1][j1].pt.x;
        center.y = keypointsBlob[1][j1].pt.y;
        PotentialBug *pb = new PotentialBug(center, frameCount);
        trackerListR.addPotentialBug(pb);
      }
    }



    imgMapped[0].copyTo(img3Lc);
    imgMapped[1].copyTo(img3Rc);

#pragma omp parallel for
    for (int ID = 0; ID < 2; ID++) {
      if (ID == 0)
        trackerList.createMatchMatrix(frameCount, initialSkip, img3L, img3R, img3Lc, img3Rc, baseFileName);
      else
        trackerListR.createMatchMatrix(frameCount, initialSkip, img3L, img3R, img3Lc, img3Rc, baseFileName);
    }



    imgMapped[0].copyTo(img3L);
    imgMapped[1].copyTo(img3R);

    char stringBuffer[100];
    sprintf(stringBuffer, " Frame %uL", frameCount+initialSkip);
    putText(img3L, stringBuffer, Point2f(100, 100), CV_FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 0, 255), 2, 8, false);
    
    sprintf(stringBuffer, " Frame %uR", frameCount+initialSkip);
    putText(img3R, stringBuffer, Point2f(100, 100), CV_FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 0, 255), 2, 8, false);   

    if (!cleanScreen) {
      if (trackerList.getNumTracks() > 0) {
        for (t2fl = 0; t2fl < trackerList.getNumTracks(); t2fl++) {
          trackerList.retraceSteps(img3L, t2fl);
        }
      }
      if (trackerListR.getNumTracks() > 0) {
        for (t2fr = 0; t2fr < trackerListR.getNumTracks(); t2fr++) {
          trackerListR.retraceSteps(img3R, t2fr);
        }
      }
      redrawKeypoints(keypointsBlob[0], img3L);
      redrawKeypoints(keypointsBlob[1], img3R);
    }

    imshow("RightCamera", img3R);
    imshow("LeftCamera", img3L);

    waitKey(3);

    if (frameCount % 1 == 0) {
      writer.write(img3L);
      writerR.write(img3R);
      Mat img;
      subtractedFrames[0].copyTo(img);
      subWriter.write(img);
    }
    //if (cvWaitKey(1 * SLOWMOTIONFACTOR) >= 0) break;
    int keyStroke;
    if (!timeProgram) {
      keyStroke = cv::waitKey(1);
    } else {
      if (timeCount < 60) {
        keyStroke = 'w';
        timeCount++;
      } else {
        keyStroke = 27;
      }
    }

    switch (keyStroke) {
      case 2424832: // left arrow
      case 65361:
      case 'q':
        frameCountL -= 1;
        if (frameCountL < 0) frameCountL = 0;
        frameCountR -= 1;
        if (frameCountR < 0) frameCountR = 0;
        frameCount -= 1;
        if (frameCount < 0) frameCount = 0;
        captureL.release();
        captureL.open(fnameL);
        captureL.set(CV_CAP_PROP_POS_FRAMES, frameCountL); //Rewind the video
        captureL.read(imgMapped[0]);
        captureR.release();
        captureR.open(fnameR);
        captureR.set(CV_CAP_PROP_POS_FRAMES, frameCountR);
        captureR.read(imgMapped[1]);
        pauser = true;
        break;
      case 2555904: //right arrow
      case 65363:
      case 'w':
        break;
      case 'f':
        bSuccess = captureR.read(imgMapped[1]);
        frameCountR++;
        pauser = true;
        break;
      case 'b':
        frameCountR--;
        if (frameCountR < 0) frameCountR = 0;
        captureR.release();
        captureR.open(fnameR);
        captureR.set(CV_CAP_PROP_POS_FRAMES, frameCountR);
        bSuccess = captureR.read(imgMapped[1]);
        pauser = true;
        break;
      case 'c':
        cleanScreen = !cleanScreen;
        pauser = true;
        break;
      case 27:
        breakLoop = true;
        break;
        //default:
        //    break;
    }
  }

  e2 = getTickCount();
  execTime = (e2 - e1) / getTickFrequency();
  printf("Execution time %7.3f \n", execTime);

  printf("\nNumber tracks %u\n", trackerList.getNumTracks());
  printf("\nNumber tracks %u\n", trackerListR.getNumTracks());
  if ((trackerList.getNumTracks() == 0) || (trackerListR.getNumTracks() == 0)) {
    puts("At least one camera shows no tracks recorded)");
    exit(0);
  }

  exit(0);
  PotentialBug pbL, pbR;
  char trackedFileName[FILENAMESIZE];
  FILE *trackedFile;
  sprintf(trackedFileName, "%s_Result.csv", baseFileName);
  trackedFile = fopen(trackedFileName, "w");
  int lfL = 0, lfR = 0;
  while (false) {
    // while (lfL < trackerList.getTrackSize(track2Follow) && lfR < trackerListR.getTrackSize(track2FollowR)) {
    //for (int j1 = 0; j1 < max(trackerList.getTrackSize(track2Follow), trackerListR.getTrackSize(track2FollowR)); j1++) {
#ifdef NOPE
    if (j1 < trackerList.getTrackSize(track2Follow)) {
      pbL = trackerList.reportTrackPosition(j1, track2Follow);
      printf("Left  %d, %f, %f \n", pbL.GetLastFrame(), pbL.GetLocation().x, pbL.GetLocation().y);
    }
    if (j1 < trackerListR.getTrackSize(track2FollowR)) {
      pbR = trackerListR.reportTrackPosition(j1, track2FollowR);
      printf("Right %d, %f, %f \n", pbR.GetLastFrame(), pbR.GetLocation().x, pbR.GetLocation().y);
    }
#endif
    pbL = trackerList.reportTrackPosition(lfL, track2Follow);
    int glfL = pbL.GetLastFrame();

    pbR = trackerListR.reportTrackPosition(lfR, track2FollowR);
    int glfR = pbR.GetLastFrame();


    if (glfL < glfR) {
      lfL++;
      continue;
    } else if (glfR < glfL) {
      lfR++;
      continue;
    }
    lfL++;
    lfR++;

    printf("Left  %d, %d, %f, %f \n", pbL.GetLastFrame() + frameBaseL, pbL.GetLastFrame(), pbL.GetLocation().x, pbL.GetLocation().y);
    printf("Right %d, %d, %f, %f \n", pbR.GetLastFrame() + frameBaseR, pbR.GetLastFrame(), pbR.GetLocation().x, pbR.GetLocation().y);

    Point3d pntLH(pbL.GetLocation().x, pbL.GetLocation().y, 1);
    Point3d pntRH(pbR.GetLocation().x, pbR.GetLocation().y, 1);
    Mat_<double>um;
    Mat X;
    um = intrinsic_matrixL.inv() * Mat_<double>(pntLH);
    pntLH = um.at<Point3d>(0);
    um = intrinsic_matrixR.inv() * Mat_<double>(pntRH);
    pntRH = um.at<Point3d>(0);

    X = IterativeLinearLSTriangulation(pntLH, Psba, pntRH, P1sba);
    printf("Triangulated %f, %f, %f\n", X.at<double>(0), X.at<double>(1), X.at<double>(2));
    fprintf(trackedFile, "%d, %d, %f, %f, ", pbL.GetLastFrame() + frameBaseL, pbL.GetLastFrame(), pbL.GetLocation().x, pbL.GetLocation().y);
    fprintf(trackedFile, "%d, %d, %f, %f, ", pbR.GetLastFrame() + frameBaseR, pbR.GetLastFrame(), pbR.GetLocation().x, pbR.GetLocation().y);
    fprintf(trackedFile, "%f, %f, %f\n", X.at<double>(0), X.at<double>(1), X.at<double>(2));
  }

  fclose(trackedFile);




  //Most experimental stuff for awhile
  Mat fmmatch;
  cv::Matx33d fmxmatch;
  if (FEATUREMATCH) { //This didn't work at all well for me
    FeatureDetector* detectorS;
    //detectorS = new SurfFeatureDetector();
    //detectorS = new MserFeatureDetector();
    //detectorS = new SiftFeatureDetector();
    //not work detectorS = new GridAdaptedFeatureDetector();
    //detectorS = new OrbFeatureDetector(200, 2, 8, 15, 0, 2, (int) ORB::HARRIS_SCORE, 31);
    /* Orb parameters
    int nfeatures=500;
    float scaleFactor=1.2f;
    int nlevels=8;
    int edgeThreshold=15; // Changed default (31);
    int firstLevel=0;
    int WTA_K=2;
    int scoreType=ORB::HARRIS_SCORE;
    int patchSize=31;
    int fastThreshold=20;   
     */

    vector<KeyPoint> keypoints1, keypoints2;
    detectorS->detect(img3L, keypoints1);
    detectorS->detect(img3R, keypoints2);

    //SurfDescriptorExtractor extractor;
    // OrbDescriptorExtractor extractor;

    Mat descriptors1, descriptors2;
    //extractor.compute(img3L, keypoints1, descriptors1);
    //extractor.compute(img3R, keypoints2, descriptors2);

    //-- Step 3: Matching descriptor vectors with a brute force matcher
    BFMatcher matcher(NORM_L2);
    std::vector< DMatch > matches;
    matcher.match(descriptors1, descriptors2, matches);

    //-- Draw matches
    Mat img_matches;
    drawMatches(img3L, keypoints1, img3R, keypoints2, matches, img_matches);

    //-- Show detected matches
    imshow("EpipolarLines", img_matches);

    waitKey(0);


    vector<Point2f>imgptsL, imgptsR;
    for (int i = 0; i < matches.size(); i++) {
      imgptsL.push_back(keypoints1[matches[i].queryIdx].pt);
      imgptsR.push_back(keypoints2[matches[i].trainIdx].pt);
    }

    fmmatch = findFundamentalMat(imgptsL, imgptsR, FM_RANSAC, 0.1, 0.99);
    cv::Matx33d fmxmatch((double*) fmmatch.clone().ptr());
    //fmxmatch = fmxmatcht;
  }


  //if (FEATUREMATCH)
  //drawEpipolarLines("EpipolarLines", fmxmatch, img3L, img3R, imagePoints2L, imagePoints2R, -1.);
  //else
  // drawEpipolarLines("EpipolarLines", fmx, img3L, img3R, imagePoints2L, imagePoints2R, -1.);



  //Mat R;
  Mat t;
  Mat E, F;
  cv::TermCriteria crit(TermCriteria::COUNT + TermCriteria::EPS, 30, 1e-6);
  //    cv::stereoCalibrate(objectPoints, points[0], points[1], 
  //    cv::stereoCalibrate(objectPoints,Points1, Points2, 
  //    cv::stereoCalibrate(objectPoints2,imagePoints[0], imagePoints[1],   
  //     cv::stereoCalibrate(objectPoints2,imagePoints3L, imagePoints3R,  
  //           intrinsic_matrixL, distortion_coeffsL, 
  //            intrinsic_matrixR, distortion_coeffsR, img3L.size(), R, t, E, F, 
  //           crit, CALIB_FIX_INTRINSIC );

  sprintf(jpegFileName, "%sL.jpg", baseFileName);
  imwrite(jpegFileName, img3L);
  sprintf(jpegFileName, "%sR.jpg", baseFileName);
  imwrite(jpegFileName, img3R);
  destroyAllWindows();
  return 0;
}



