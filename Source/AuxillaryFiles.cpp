/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

#include "opencv2/opencv.hpp"
#include <vector>
#include <cmath>
#include <omp.h>
using namespace cv;
using namespace std;

#include "Auxillary.h"

void updateDensity(Mat density, Mat img) {
  #pragma omp parallel for collapse(2)
  for (unsigned int i = 0; i < img.cols; i++) {
    for (unsigned int u = 0; u < img.rows; u++) {
      if (img.at<uchar>(u, i) != 0) {
        density.at<unsigned int>(u, i) += 1;
      }
    }
  }
}

void densityMapping(Mat density, Mat img) {
  unsigned int maxPixel = 0;
  //  int p = getNumThreads();
#pragma omp parallel for reduction(max : maxPixel)  
  for (unsigned int i = 0; i < img.cols; i++) {
    for (unsigned int u = 0; u < img.rows; u++) {
      if (density.at<unsigned int>(u, i) > maxPixel) {
        maxPixel = density.at<unsigned int>(u, i);
      }
    }
  }
  //printf("Maxpixel was %d \n", maxPixel);



#pragma omp parallel for
  for (unsigned int i = 0; i < img.cols; i++) {
    for (unsigned int u = 0; u < img.rows; u++) {
      img.at<uchar>(u, i) = (uchar) ((double) density.at<unsigned int>(u, i) / maxPixel * 255);
    }
  }
}



void printBackgroundValues(Ptr<BackgroundSubtractorMOG2> bgR) {
  printf("\nbgRatio %f \n", bgR->getBackgroundRatio()); //   getDouble("backgroundRatio"));
  printf("Nmixes %d\n", bgR->getNMixtures()); //.getInt("nmixtures"));
  printf("History %d\n", bgR->getHistory()); //getInt("history"));
  printf("fCT %f\n", bgR->getComplexityReductionThreshold());
  printf("Varthresh %f\n", bgR->getVarThresholdGen());
}

char *ret_base_fname(char *arg, char * baseFileName, char * extension) {
  int back, j1, extlen;
  //char exten[250];

  back = (int) strlen(arg);

  for (j1 = back - 1; j1 >= 0; j1--) {
    if (arg[j1] == '.') {
      strncpy(baseFileName, arg, j1);
      baseFileName[j1] = 0;
      extlen = strlen(arg) - j1;

      memcpy(extension, &arg[j1], extlen);
      extension[extlen] = '\0';

      return baseFileName;
    }
  }
  return strcpy(baseFileName, arg);
}

void DecomposeEssential(Mat Essential, Mat& R1, Mat& R2, Mat& t1, Mat& t2) {
  //SVD decomp = SVD(Essential, SVD::MODIFY_A);
  SVD decomp = SVD(Essential);
  Mat U = decomp.u;

  //S
  Mat S(3, 3, CV_64F, Scalar(0));
  S.at<double>(0, 0) = decomp.w.at<double>(0, 0);
  S.at<double>(1, 1) = decomp.w.at<double>(0, 1);
  S.at<double>(2, 2) = decomp.w.at<double>(0, 2);


  //V
  Mat V = decomp.vt.t(); //Needs to be decomp.vt.t(); (transpose once more)

  //W
  //Mat W(3, 3, CV_64F, Scalar(0));
  //W.at<double>(0, 1) = -1;
  //W.at<double>(1, 0) = 1;
  //W.at<double>(2, 2) = 1;

  Matx33d W(0, -1, 0, //HZ 9.13
          1, 0, 0,
          0, 0, 1);
  Matx33d Wt(0, 1, 0,
          -1, 0, 0,
          0, 0, 1);
  R1 = U * Mat(W) * decomp.vt; //HZ 9.19
  R2 = U * Mat(Wt) * decomp.vt; //HZ 9.19
  t1 = U.col(2); //u3
  t2 = -U.col(2); //u3
  //cout << "computed rotation: " << endl;
  //cout << R1 << endl;
  //cout << "computed rotation: " << endl;
  //cout << R2 << endl;
  //cout << "Translation to Right" << endl << t1 << endl;
  //cout << "Translation to Right1" << endl << t2 << endl;
  //rot = U * W * V.t();
  //tvec = U.col(2);
  return;
}

bool CheckCoherentRotation(cv::Mat& R) {


  if (fabsf(determinant(R)) - 1.0 > 1e-07) {
    cerr << "det(R) != +-1.0, this is not a rotation matrix" << endl;
    return false;
  }

  return true;
}

bool triangulate(vector<Point2f>imgPntsL, vector<Point2f>imgPntsR, Mat P, Mat P1, Mat intrinsicL, Mat intrinsicR) {
  for (int j = 0; j < imgPntsL.size(); j++) {
    Point3d pntLH(imgPntsL[j].x, imgPntsL[j].y, 1);
    Point3d pntRH(imgPntsR[j].x, imgPntsR[j].y, 1);

    Mat_<double> um = intrinsicL.inv() * Mat_<double>(pntLH);
    pntLH = um.at<Point3d>(0);
    um = intrinsicR.inv() * Mat_<double>(pntRH);
    pntRH = um.at<Point3d>(0);

    Mat X = IterativeLinearLSTriangulation(pntLH, P, pntRH, P1);
    if (X.at<double>(2) < 0)
      return false;
  }
  return true;
}

bool testRotationMatrices(Mat& E, Mat& R, Mat& R3, Mat& t, Mat& Pret, vector<Point2f>imgPntsL, vector<Point2f>imgPntsR, Mat intrinsicL, Mat intrinsicR) {

  // cout << "Testing P1 " << endl << Mat(P2) << endl;
  Mat R1, R2;
  Mat t1, t2;

  DecomposeEssential(E, R1, R2, t1, t2);


  if (determinant(R1) + 1.0 < 1e-09) {
    //according to http://en.wikipedia.org/wiki/Essential_matrix#Showing_that_it_is_valid
    cout << "det(R) == -1 [" << determinant(R1) << "]: flip E's sign" << endl;
    E = -E;
    DecomposeEssential(E, R1, R2, t1, t2);
  }
  Mat P = Mat::eye(3, 4, CV_64F);
  Mat P2 = createPMatrix2(R1, t1);
  Mat P21 = createPMatrix2(R1, t2);
  Mat P22 = createPMatrix2(R2, t1);
  Mat P23 = createPMatrix2(R2, t2);


  if (triangulate(imgPntsL, imgPntsR, P, P2, intrinsicL, intrinsicR)) {
    R = R1;
    R3 = R2;
    t = t1;
    Pret = P2;
    return true;
  } else if (triangulate(imgPntsL, imgPntsR, P, P21, intrinsicL, intrinsicR)) {
    R = R1;
    R3 = R2;
    t = t2;
    Pret = P21;
    return true;
  } else if (triangulate(imgPntsL, imgPntsR, P, P22, intrinsicL, intrinsicR)) {
    R = R2;
    R3 = R1;
    t = t1;
    Pret = P22;
    return true;
  } else if (triangulate(imgPntsL, imgPntsR, P, P23, intrinsicL, intrinsicR)) {
    R = R2;
    R3 = R1;
    t = t2;
    Pret = P23;
    return true;
  }
  return false;
}

Mat EssentialFromFundamental(Mat F, Mat IntL, Mat IntR) {
  Mat Essen;
  Essen = IntR.t() * F * IntL;
  return Essen;
}

void printMatrix(const cv::Mat &M, std::string matrix) {
  printf("Matrix \"%s\" is %i x %i\n", matrix.c_str(), M.rows, M.cols);
  for (int j1 = 0; j1 < M.rows; j1++) {
    for (int j2 = 0; j2 < M.cols; j2++) {
      printf(" %6.4f ", M.at<double>(j1, j2));
    }
    printf("\n");
  }
  printf("\n");
}

Mat createPMatrix(Mat rMat, Mat tvec) {
  Mat P;
  P.create(3, 4, cv::DataType<double>::type);
  for (int j1 = 0; j1 < 3; j1++) {
    for (int j2 = 0; j2 < 3; j2++) {
      P.at<double>(j1, j2) = rMat.at<double>(j1, j2);
    }
    P.at<double>(j1, 3) = tvec.at<double>(0, j1);
  }
  return P;
}

Mat createPMatrix2(Mat rMat, Mat tvec) {
  Mat P;
  P.create(3, 4, cv::DataType<double>::type);
  for (int j1 = 0; j1 < 3; j1++) {
    for (int j2 = 0; j2 < 3; j2++) {
      P.at<double>(j1, j2) = rMat.at<double>(j1, j2);
    }
    P.at<double>(j1, 3) = tvec.at<double>(j1, 0);
  }
  return P;
}

Mat createEssentialMatrix(Mat rMat, Mat tvec) {
  Mat cpT(3, 3, CV_64FC1);

  //This creates cross product matrix of tvec
  //This doesn't seem to be working
  cpT.at<double>(0, 0) = 0;
  cpT.at<double>(0, 1) = -tvec.at<double>(0, 3);
  cpT.at<double>(0, 2) = tvec.at<double>(0, 2);
  cpT.at<double>(1, 0) = tvec.at<double>(0, 3);
  cpT.at<double>(1, 1) = 0;
  cpT.at<double>(1, 2) = -tvec.at<double>(0, 1);
  cpT.at<double>(2, 0) = -tvec.at<double>(0, 2);
  cpT.at<double>(2, 1) = tvec.at<double>(0, 1);
  cpT.at<double>(2, 2) = 0;
  Mat ret = (cpT * rMat);
  return (cpT * rMat); //This was rMat * cpT
}


/**
 From "Triangulation", Hartley, R.I. and Sturm, P., Computer vision and image understanding, 1997
 */
#define EPSILON 0.00001

Mat_<double> LinearLSTriangulation(Point3d u, //homogenous image point (u,v,1)
        Matx34d P, //camera 1 matrix
        Point3d u1, //homogenous image point in 2nd camera
        Matx34d P1 //camera 2 matrix
        ) {

  //build matrix A for homogenous equation system Ax = 0
  //assume X = (x,y,z,1), for Linear-LS method
  //which turns it into a AX = B system, where A is 4x3, X is 3x1 and B is 4x1

  Matx43d A(u.x * P(2, 0) - P(0, 0), u.x * P(2, 1) - P(0, 1), u.x * P(2, 2) - P(0, 2),
          u.y * P(2, 0) - P(1, 0), u.y * P(2, 1) - P(1, 1), u.y * P(2, 2) - P(1, 2),
          u1.x * P1(2, 0) - P1(0, 0), u1.x * P1(2, 1) - P1(0, 1), u1.x * P1(2, 2) - P1(0, 2),
          u1.y * P1(2, 0) - P1(1, 0), u1.y * P1(2, 1) - P1(1, 1), u1.y * P1(2, 2) - P1(1, 2)
          );
  Matx41d B(-(u.x * P(2, 3) - P(0, 3)),
          -(u.y * P(2, 3) - P(1, 3)),
          -(u1.x * P1(2, 3) - P1(0, 3)),
          -(u1.y * P1(2, 3) - P1(1, 3)));

  Mat_<double> X;
  solve(A, B, X, DECOMP_SVD);

  return X;
}

/**
 From "Triangulation", Hartley, R.I. and Sturm, P., Computer vision and image understanding, 1997
 */
Mat_<double> IterativeLinearLSTriangulation(Point3d u, //homogenous image point (u,v,1)
        Matx34d P, //camera 1 matrix
        Point3d u1, //homogenous image point in 2nd camera
        Matx34d P1 //camera 2 matrix
        ) {
  double wi = 1, wi1 = 1;
  Mat_<double> X(4, 1);

  Mat_<double> X_ = LinearLSTriangulation(u, P, u1, P1);
  X(0) = X_(0);
  X(1) = X_(1);
  X(2) = X_(2);
  X(3) = 1.0;

  for (int i = 0; i < 10; i++) { //Hartley suggests 10 iterations at most		

    //recalculate weights
    double p2x = Mat_<double>(Mat_<double>(P).row(2) * X)(0);
    double p2x1 = Mat_<double>(Mat_<double>(P1).row(2) * X)(0);

    //breaking point
    if (fabsf(wi - p2x) <= EPSILON && fabsf(wi1 - p2x1) <= EPSILON) break;

    wi = p2x;
    wi1 = p2x1;

    //reweight equations and solve
    Matx43d A((u.x * P(2, 0) - P(0, 0)) / wi, (u.x * P(2, 1) - P(0, 1)) / wi, (u.x * P(2, 2) - P(0, 2)) / wi,
            (u.y * P(2, 0) - P(1, 0)) / wi, (u.y * P(2, 1) - P(1, 1)) / wi, (u.y * P(2, 2) - P(1, 2)) / wi,
            (u1.x * P1(2, 0) - P1(0, 0)) / wi1, (u1.x * P1(2, 1) - P1(0, 1)) / wi1, (u1.x * P1(2, 2) - P1(0, 2)) / wi1,
            (u1.y * P1(2, 0) - P1(1, 0)) / wi1, (u1.y * P1(2, 1) - P1(1, 1)) / wi1, (u1.y * P1(2, 2) - P1(1, 2)) / wi1
            );
    Mat_<double> B = (Mat_<double>(4, 1) << -(u.x * P(2, 3) - P(0, 3)) / wi,
            -(u.y * P(2, 3) - P(1, 3)) / wi,
            -(u1.x * P1(2, 3) - P1(0, 3)) / wi1,
            -(u1.y * P1(2, 3) - P1(1, 3)) / wi1
            );

    solve(A, B, X_, DECOMP_SVD);
    X(0) = X_(0);
    X(1) = X_(1);
    X(2) = X_(2);
    X(3) = 1.0;
  }
  return X;
}

float euclidDistance(Point2f p1, Point2f p2) {
  float sum;
  sum = (p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) *(p1.y - p2.y);
  return (sqrt(sum));
}

int randomInt(int min, int max) {
  return (min + (rand() % (int) (max - min + 1)));
}

double scaleRecovery(vector<Point3f> measured, vector<Point3f> actual) {
  //Mat Nbar = {0, 0, 0};
  //Mat Mbar = {0, 0, 0};
  Mat Nbar{0, 0, 0};
  Mat Mbar{0, 0, 0};
  
  if (measured.size() != actual.size()) {
    printf("Size of measured points is not equal to actual\n");
    return -1;
  }

  for (int j = 0; j < measured.size(); j++) {
    Nbar = Nbar + (Mat) measured[j];
    Mbar = Mbar + (Mat) actual[j];
  }

  Nbar = Nbar / measured.size();
  Mbar = Mbar / measured.size();

  double Np, Mp;

  //Absolute value of a vector means taking second norm of the vector i.e. ∥x∥. 
  //That means the same thing as x21+x22+...+x2n‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾√. I don't understand 
  //   that is sqrt(x1^2+ x2^2+...+xn^2)
  //why some top researchers in computer science abuse the notation where |.|
  //is widely used for absolute value of scalars in math.
  //Edit: Recently, I read some paper using |x|
  //to mean absolute value of each component of the vector x. In summary, meaning
  //will depends upon the context and author.
  Mat C = Mat::zeros(3, 3, CV_64F);
  for (int j = 0; j < measured.size(); j++) {
    Mat tmp = (Mat) measured[j] - Nbar;
    Np += sqr(tmp.at<float>(0)) + sqr(tmp.at<float>(1)) + sqr(tmp.at<float>(2));
    Mat tmp2 = (Mat) measured[j] - Mbar;
    Mp += sqr(tmp2.at<float>(0)) + sqr(tmp2.at<float>(1)) + sqr(tmp2.at<float>(2));
    //Np = Np + sqr((Mat) measured[j] - Nbar);
    //Mp = Mp + sqr((Mat) actual[j] - Mbar);
    C = C + tmp * tmp2.t();
  }

  SVD decomp = SVD(C);
  Mat U = decomp.u;
  //V
  Mat V = decomp.vt.t();

  Mat Rot2 = V * U.t();

  double scale = Mp / Np;

  Mat t = Nbar - scale * Rot2 * Mbar;
  //scale.at<float>(0) = sqrt(scale.at<float>(0));
  //scale.at<float>(1) = sqrt(scale.at<float>(1));
  //scale.at<float>(2) = sqrt(scale.at<float>(2));
  return scale;
}

