/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Track.h
 * Author: mcaprio3
 *
 * Created on July 1, 2016, 11:27 AM
 */
#include "opencv2/opencv.hpp"
//#include </usr/local/include/opencv2/opencv.hpp>
//#include "opencv2/nonfree/nonfree.hpp"

#ifndef TRACK_H
#define TRACK_H

class Track {
public:
    Track();
    Track(double p1, double p2, double p3, double v1, double v2, double v3);
    Track(double p1, double p2, double p3, cv::Matx31d vx); 
    Track(const Track& orig);
    virtual ~Track();
    
    cv::Matx31d P0;
    cv::Matx31d v;
private:

};

#endif /* TRACK_H */

