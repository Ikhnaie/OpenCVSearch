/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Deinterlace.h
 * Author: Mike
 *
 * Created on May 31, 2016, 6:21 PM
 */
#include <stdlib.h>
#include <stdio.h>
//#include <highgui.h>
#include "opencv2/opencv.hpp"
//#include </usr/local/include/opencv2/opencv.hpp>
//#include "opencv2/nonfree/nonfree.hpp"

using namespace cv;
using namespace std;

#ifndef DEINTERLACE_H
#define DEINTERLACE_H

class Deinterlace {
public:
    Deinterlace();
    Deinterlace(const Deinterlace& orig);
    virtual ~Deinterlace();
    
    void deinterlaceImage(cv::Mat, cv::Mat, bool even);
private:

};

#endif /* DEINTERLACE_H */

