/* 
 * File:   tracker.h
 * Author: Mike
 *
 * Created on June 22, 2014, 11:08 AM
 */

#include <stdlib.h>
#include <stdio.h>
#include "opencv2/opencv.hpp"
//#include </usr/local/include/opencv2/opencv.hpp>
//#include "opencv2/nonfree/nonfree.hpp"
#include <cmath>
#include <cstdio>
#include <list>
#include <omp.h>

#include "PotentialBug.h"
#include "LineDesc.h"
#include "GlobalDefinitions.h"
#include "CommonParameters.h"
using namespace cv;
using namespace std;

#ifndef TRACKER_H
#define TRACKER_H

class tracker {
public:
  tracker();
  tracker(const tracker& orig);
  virtual ~tracker();
  tracker(Mat *imgOut);

  void evaluatePoint(PotentialBug *pb, unsigned int frameCount, int track2Follow);
  void cleanupPotentialBugs(unsigned int currentFrame);
  virtual void writeATrack(int index, int mAvr, double fps, char * baseFileName, char *trackname);
  virtual void followATrack(int index);
  int getNumTracks();
  unsigned int getTrackSize(unsigned int j);
  unsigned int getInitialTrackFrame(unsigned int j);
  unsigned int getFinalTrackFrame(unsigned int j);
  double getTrackDistance(unsigned int j, int mAvr);
  double getTrackDegreesTurned(unsigned int j, int mAvr);
  double getTrackAverageDegreesTurned(unsigned int j, int mAvr);
  double getTrackAverageDistance(unsigned int j, int mAvr);
  int findNearestEndofTrackToPoint(Point2f p);
  unsigned int findNearestTrackToPoint(Point2f p);
  list<PotentialBug>::iterator findNearestNeighbor2EndOfTrack(double *MinDis2, int track2Follow);
  list<PotentialBug>::iterator findNearestNeighbor(PotentialBug& pb, double *MinDis2, double current, bool newPoint);
  void repairTrack(int, Point2f, unsigned int);
  double getPointDegreesTurned(unsigned int j, unsigned int k, int mAvr);
  double getPointSignedDegreesTurned(unsigned int j, unsigned int j1, int mAvr);
  double getTrackUnweightedDegreesTurned(unsigned int j, int mAvr);
  double getTrackAverageUnweighterDegreesTurned(unsigned int j, int mAvr);
  void retraceSteps(Mat img3, int track2Follow);
  void retraceSteps2(Mat img3, vector<PotentialBug> &OneBug);
  int createNewTrack(Point2f p, int frame);
  void removeLastPoint(int track2Follow);
  Point2f getLastLocation(int track2Follow);
  void addPotentialBug(PotentialBug *pb);
  void extendTrack(PotentialBug pb, int track2Follow, int frameCount);
  PotentialBug reportTrackPosition(int index, int track2Follow);
  void setCPs1(CommonParameters CPs1);
  CommonParameters getCPs1() const;
  int getNumPotentialBugs();
  void removeBug(list<PotentialBug>::iterator list_iterator);
  void convertPBs2Tracks(int frameCount);
  //void removeOldTracks(int frameCount, Mat imgL, Mat imgR,char *baseFileName);
  void removeOldTracks(int frameCount, int iSkip, Mat imgL, Mat imgR, Mat imgLc, Mat imgRc, char *baseFileName);
  //void createMatchMatrix(int frameCount, Mat imgL, Mat imgR,char *baseFileName);
  void createMatchMatrix(int frameCount, int iSkip, Mat imgL, Mat imgR, Mat img3Lc, Mat img3Rc, char *baseFileName);
  double calcDirectedness(vector<PotentialBug> &bugTrack);
  double calcDirectednessStep(vector<PotentialBug> &bugTrack, int steps);
  double calcLDisplaceVStep(vector<PotentialBug> &bugTrack, int step);  
  double calcTurnRateStep(vector<PotentialBug> &bugTrack, int step);
  double calcTurnRateStep2(vector<PotentialBug> &bugTrack, int step);   
  void setID(int ID);
  int getID() const;

protected:
  Mat *img;
  vector<vector<PotentialBug> > tracks;
  //vector<vector<double> > matchMatrix;


private:
  list<PotentialBug> potentialBugs;
  int nTracks;
  unsigned int nPointsInTrack;
  int randomInt(int min, int max);
  CommonParameters CPs1;
  int ID;
};

#endif /* TRACKER_H */

