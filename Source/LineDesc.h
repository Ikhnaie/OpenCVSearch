/* 
 * File:   LineDesc.h
 * Author: Mike
 *
 * Created on June 8, 2014, 11:54 AM
 */
#include <stdlib.h>
#include <stdio.h>
//#include <highgui.h>
#include "opencv2/opencv.hpp"
//#include </usr/local/include/opencv2/opencv.hpp>
//#include "opencv2/nonfree/nonfree.hpp"
#include <cmath>

#ifndef LINEDESC_H
#define	LINEDESC_H

using namespace cv;

class LineDesc {
public:
    LineDesc();
    LineDesc(const LineDesc& orig);
    virtual ~LineDesc();
    void buildLine(Point2f new1, Point2f new2); 
    double distanceToPoint(Point2f point2);

    void setB1(double b1) {
        this->b1 = b1;
    }

    double getB1() const {
        return b1;
    }

    void setM1(double m1) {
        this->m1 = m1;
    }

    double getM1() const {
        return m1;
    }
private:
    Point2f firstPoint, secondPoint;
    double m1; //slope of line
    double b1; //y intersect of line
    
    void calcSlopeAndIntersect();
};

#endif	/* LINEDESC_H */

