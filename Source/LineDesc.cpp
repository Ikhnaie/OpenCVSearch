/* 
 * File:   LineDesc.cpp
 * Author: Mike
 * 
 * Created on June 8, 2014, 11:54 AM
 */
#include <stdlib.h>
#include <stdio.h>
//#include <highgui.h>
#include <opencv2/opencv.hpp>
//#include </usr/local/include/opencv2/opencv.hpp>
//#include "opencv2/nonfree/nonfree.hpp"
#include <cmath>
#include "LineDesc.h"

LineDesc::LineDesc() {
}

LineDesc::LineDesc(const LineDesc& orig) {
}

LineDesc::~LineDesc() {
}

void LineDesc::buildLine(Point2f new1, Point2f new2){
    firstPoint = new1;
    secondPoint = new2;
    calcSlopeAndIntersect();
}

double LineDesc::distanceToPoint(Point2f point2){
    double m2;
    double b2;
    Point2f intersection;
    double distance;
       
    m2 = -1.0/m1;  //this is slope of normal to line  
    b2 = point2.y - (m2 * point2.x);  // y intersect of normal line
    intersection.x = (b1 - b2)/(m2 - m1);
    intersection.y = m1 * intersection.x + b1;
    distance = sqrt((point2.x - intersection.x) * (point2.x - intersection.x) + (point2.y - intersection.y) * (point2.y - intersection.y));
    
    return distance;
}

void LineDesc::calcSlopeAndIntersect(){
    m1 = (firstPoint.y - secondPoint.y)/(firstPoint.x - secondPoint.x);
    b1 = firstPoint.y - (m1 * firstPoint.x); //y intersect of our line
}

