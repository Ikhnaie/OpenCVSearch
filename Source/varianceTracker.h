/* 
 * File:   varianceTracker.h
 * Author: mcaprio2
 *
 * Created on June 9, 2014, 2:52 PM
 */
#include <stdlib.h>
#include <stdio.h>
//#include <highgui.h>
#include "opencv2/opencv.hpp"
//#include </usr/local/include/opencv2/opencv.hpp>
//#include "opencv2/nonfree/nonfree.hpp"
#include <cmath>

#ifndef VARIANCETRACKER_H
#define	VARIANCETRACKER_H

using namespace cv;

class varianceTracker {
public:
  varianceTracker();
  varianceTracker(const varianceTracker& orig);
  virtual ~varianceTracker();
  double addPoint(Point2f p1);
  
  int buffSize;
  double *Carray;
 
  
private:
  int current;
  int total;
  Point2f prevPoint;

};

#endif	/* VARIANCETRACKER_H */

