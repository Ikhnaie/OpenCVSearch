/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Track.cpp
 * Author: mcaprio3
 * 
 * Created on July 1, 2016, 11:27 AM
 */

#include "Track.h"

Track::Track() {
}

Track::Track(double p1, double p2, double p3, double v1, double v2, double v3) {
     P0(0,0) = (p1);
    P0(1,0) = p2;
    P0(2,0) = p3;
     v(0,0) = v1;
    v(1,0) = v2;
    v(2,0) = v3; 
}

Track::Track(double p1, double p2, double p3, cv::Matx31d vx) {
     P0(0,0) = (p1);
    P0(1,0) = p2;
    P0(2,0) = p3;
     v(0,0) = vx(1,0);
    v(1,0) = vx(2,0);
    v(2,0) = vx(3,0); 
}


Track::Track(const Track& orig) {
    P0 = orig.P0;
    v = orig.v;
}

Track::~Track() {
}

