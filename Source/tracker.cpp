/* 
 * File:   tracker.cpp
 * Author: Mike
 * 
 * Created on June 22, 2014, 11:08 AM
 */

#include "tracker.h"

#define sqr(a)  ((a) * (a))

unsigned int tracker::findNearestTrackToPoint(Point2f p) {
  vector<PotentialBug> OneBug;
  double MinDis = 10000000;
  double distance;
  unsigned int trackIndex;
  for (int j = 0; j < tracks.size(); j++) {
    OneBug = tracks[j];
    for (int k = 0; k < OneBug.size() - 1; k++) {
      distance = sqr(OneBug[k].GetLocation().x - p.x) + sqr(OneBug[k].GetLocation().y - p.y);
      if (distance < MinDis) {
        trackIndex = j;
        MinDis = distance;
      }
    }
  }
  return trackIndex;
}

void tracker::createMatchMatrix(int frameCount, int iSkip, Mat imgL, Mat imgR, Mat imgLc, Mat imgRc, char *baseFileName) {
  vector<vector<double> > matchMatrix;

  matchMatrix.resize(getNumTracks());
  double minDis = 0;

  for (int j = 0; j < getNumTracks(); j++) {
    matchMatrix[j].resize(this->getNumPotentialBugs());
    int k1 = 0;
    for (auto k = potentialBugs.begin(); k != potentialBugs.end(); k++) {
      matchMatrix[j][k1] = sqrt(sqr(getLastLocation(j).x - k->GetLocation().x) + sqr(getLastLocation(j).y - k->GetLocation().y));
      k1++;
    }
  }

  list<PotentialBug>::iterator kitStore;
  if (getNumTracks() > 0) {
    do {
      minDis = CPs1.GetTrackerMinSize();
      int j1, k1, kmax = potentialBugs.size();
      for (int j = 0; j < getNumTracks(); j++) {
        list<PotentialBug>::iterator kit = potentialBugs.begin();
        for (int k = 0; k < kmax; k++, kit++) {
          if (matchMatrix[j][k] < minDis) {
            minDis = matchMatrix[j][k];
            j1 = j;
            k1 = k;
            kitStore = kit;
          }
        }
      }

      //Need to extend track and release pB here
      if (minDis < CPs1.GetTrackerMinSize()) {
        kitStore->SetTracked(true);
        extendTrack(*kitStore, j1, frameCount);

        double maxVal = CPs1.GetTrackerMinSize();
        for (int j = 0; j < getNumTracks(); j++)
          matchMatrix[j][k1] = maxVal;
        if (getNumTracks() > 0) {
          for (int k = 0; k < kmax; k++)
            matchMatrix[j1][k] = maxVal;
        }
      }
    } while (minDis < CPs1.GetTrackerMinSize());
  }

  //Convert untracked pbs to new tracks
  convertPBs2Tracks(frameCount);
  cleanupPotentialBugs(frameCount);

  //Remove old tracks
  removeOldTracks(frameCount, iSkip, imgL, imgR, imgLc, imgRc, baseFileName);
  return;
}

void tracker::setID(int ID) {
  this->ID = ID;
}

int tracker::getID() const {
  return ID;
}

int tracker::randomInt(int min, int max) {
  return (min + (rand() % (int) (max - min + 1)));
}

tracker::tracker() {
  nTracks = 0;
  ID = -1;
}

tracker::tracker(Mat *imgOut) {
  nTracks = 0;
  img = imgOut;
  ID = -1;
}

tracker::tracker(const tracker& orig) {
  CPs1 = orig.CPs1;
  nTracks = 0;
  ID = -1;
}

tracker::~tracker() {
}

void tracker::retraceSteps(Mat img3, int track2Follow) {
  vector<PotentialBug> OneBug;
  if (track2Follow >= 0) {
    OneBug = tracks[track2Follow];
    for (int j = 1; j < OneBug.size(); j++) {
      line(img3, OneBug[j].GetLocation(), OneBug[j - 1].GetLocation(), OneBug[0].GetLineColor(), 2, 8);
      if (j < OneBug.size() - 1)
        circle(img3, OneBug[j].GetLocation(), CPs1.GetTrackCircleSize(), Scalar(0, 255, 0), 2, 8, 0);
    }
  }
}

void tracker::retraceSteps2(Mat img3, vector<PotentialBug>& OneBug) {

  for (int j = 1; j < OneBug.size(); j++) {
    line(img3, OneBug[j].GetLocation(), OneBug[j - 1].GetLocation(), Scalar(100, 255, 0), 2, 8);
    if (j < OneBug.size() - 1)
      circle(img3, OneBug[j].GetLocation(), CPs1.GetTrackCircleSize(), Scalar(0, 255, 0), 2, 8, 0);
  }
}

Point2f tracker::getLastLocation(int track2Follow) {
  vector<PotentialBug> OneBug;
  if (track2Follow >= 0) {
    OneBug = tracks[track2Follow];
    if (OneBug.size() > 0)
      return (OneBug[OneBug.size() - 1].GetLocation());
  }
}

void tracker::extendTrack(PotentialBug pb, int track2Follow, int frameCount) {
  if (track2Follow >= 0) {
    pb.SetIndex(track2Follow);
    pb.SetLastFrame(frameCount);
    tracks[track2Follow].push_back(pb);
  }
}

double tracker::calcDirectedness(vector<PotentialBug> &bugTrack) {
  double directDisplacement, flownDisplacement;
  int bugSize = bugTrack.size();
  directDisplacement = cv::norm(bugTrack[0].GetLocation() - bugTrack[bugSize - 1].GetLocation());
  flownDisplacement = 0.0;
  for (int j = 1; j < bugSize; j++) {
    flownDisplacement += cv::norm(bugTrack[j].GetLocation() - bugTrack[j - 1].GetLocation());
  }
  return flownDisplacement / directDisplacement;
}

double tracker::calcDirectednessStep(vector<PotentialBug> &bugTrack, int step) {
  double directDisplacement, flownDisplacement, bestDirectedness;
  int bugSize = bugTrack.size();
  bestDirectedness = 1000.0;
  if (bugSize >= step) {
    for (int j = step; j <= bugSize; j++) {
      directDisplacement = cv::norm(bugTrack[j].GetLocation() - bugTrack[j - step + 1].GetLocation());
      flownDisplacement = 0.0;
      for (int j1 = 0; j1 < step - 1; j1++) {
        flownDisplacement += cv::norm(bugTrack[j - j1].GetLocation() - bugTrack[j - j1 - 1].GetLocation());
      }
      if (flownDisplacement / directDisplacement < bestDirectedness)
        bestDirectedness = flownDisplacement / directDisplacement;
    }
  }
  if (bestDirectedness == 1000.0)
    return sqrt(-1);
  else
   return bestDirectedness;
}

double tracker::calcLDisplaceVStep(vector<PotentialBug> &bugTrack, int step) {
  double displaceV, sumx, sumx2, bestLDisplaceV;
  int bugSize = bugTrack.size();
  int cnt;
  bestLDisplaceV = 10000.0;
  if (bugSize >= step) {
    for (int j = step; j <= bugSize; j++) {
      sumx = sumx2 = cnt = 0;
      for (int j1 = 0; j1 < step - 1; j1++) {
        if (bugTrack[j - j1].GetLastFrame() - bugTrack[j - j1 - 1].GetLastFrame() == 1) {
          cnt++;
          sumx += cv::norm(bugTrack[j - j1].GetLocation() - bugTrack[j - j1 - 1].GetLocation());
          sumx2 += sqr(cv::norm(bugTrack[j - j1].GetLocation() - bugTrack[j - j1 - 1].GetLocation()));
        }
        if (cnt > 1) {
          double var = (sumx2 - sqr(sumx) / cnt) / (cnt - 1);
          if (var < bestLDisplaceV)
            bestLDisplaceV = var;
        }
      }
    }
  }
  if (bestLDisplaceV == 10000)
    return sqrt(-1);
  else
    return sqrt(bestLDisplaceV);
}

double tracker::calcTurnRateStep(vector<PotentialBug> &bugTrack, int step) {
  double turnRate, bestTurnRate, turnRateSum;
  //int firstPoint, hingePoint;
  double degreesTurned;
  double x, y, z, tmp;
  int bugSize = bugTrack.size();
  int cnt;
  bestTurnRate = 10000.0;
  if (bugSize >= step) {
    for (int j = step; j <= bugSize; j++) {
      cnt = 0;
      turnRateSum = 0.0;
      for (int j1 = 0; j1 < step - 1; j1++) {
        if (bugTrack[j - j1].GetLastFrame() - bugTrack[j - j1 - 2].GetLastFrame() == 2) {
          cnt++;
          x = sqrt(sqr(bugTrack[j - j1 - 1].GetLocation().x - bugTrack[j - j1 - 2].GetLocation().x) + sqr(bugTrack[j - j1 - 1].GetLocation().y - bugTrack[j - j1 - 2].GetLocation().y));
          y = sqrt(sqr(bugTrack[j - j1 - 1].GetLocation().x - bugTrack[j - j1].GetLocation().x) + sqr(bugTrack[j - j1 - 1].GetLocation().y - bugTrack[j - j1].GetLocation().y));
          z = sqrt(sqr(bugTrack[j - j1].GetLocation().x - bugTrack[j - j1 - 2].GetLocation().x) + sqr(bugTrack[j - j1].GetLocation().y - bugTrack[j - j1 - 2].GetLocation().y));

          //acos domain is -1 to 1
          if (abs(z - (x + y)) < 0.0001) //There was no turn
            turnRate = 0.0;
          if (z == abs(x - y)) //180 degree turn
            turnRate = 180;
          //   return 180 * (x / (OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame()) +
          //         y / (OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame()));
          tmp = (sqr(x) + sqr(y) - sqr(z)) / (2 * x * y);
          if (tmp <= 1.0 && tmp >= -1.0)
            turnRate = 180 - (acos(tmp) * 180 / 3.14159);
          turnRateSum += turnRate;

        }
      }
      if (cnt > 1) {
        turnRate = turnRateSum / cnt;
        if (turnRate < bestTurnRate)
          bestTurnRate = turnRate;
      }
    }
  }
  if (bestTurnRate == 10000.0)
    return sqrt(-1);
  return bestTurnRate;
}


double tracker::calcTurnRateStep2(vector<PotentialBug> &bugTrack, int step) {
  double turnRate, bestTurnRate, turnRateSum;
  //int firstPoint, hingePoint;
  double degreesTurned;
  double x, y, z, tmp;
  int bugSize = bugTrack.size();
  int cnt;
  bestTurnRate = 10000.0;
  if (bugSize >= step+2) {
    for (int j = step+2; j <= bugSize; j++) {
      cnt = 0;
      turnRateSum = 0.0;
      for (int j1 = 0; j1 < step; j1++) {
        if (bugTrack[j - j1].GetLastFrame() - bugTrack[j - j1 - 2].GetLastFrame() == 2) {
          cnt++;
          x = sqrt(sqr(bugTrack[j - j1 - 1].GetLocation().x - bugTrack[j - j1 - 2].GetLocation().x) + sqr(bugTrack[j - j1 - 1].GetLocation().y - bugTrack[j - j1 - 2].GetLocation().y));
          y = sqrt(sqr(bugTrack[j - j1 - 1].GetLocation().x - bugTrack[j - j1].GetLocation().x) + sqr(bugTrack[j - j1 - 1].GetLocation().y - bugTrack[j - j1].GetLocation().y));
          z = sqrt(sqr(bugTrack[j - j1].GetLocation().x - bugTrack[j - j1 - 2].GetLocation().x) + sqr(bugTrack[j - j1].GetLocation().y - bugTrack[j - j1 - 2].GetLocation().y));

          //acos domain is -1 to 1
          if (abs(z - (x + y)) < 0.0001) //There was no turn
            turnRate = 0.0;
          if (z == abs(x - y)) //180 degree turn
            turnRate = 180;
          //   return 180 * (x / (OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame()) +
          //         y / (OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame()));
          tmp = (sqr(x) + sqr(y) - sqr(z)) / (2 * x * y);
          if (tmp <= 1.0 && tmp >= -1.0)
            turnRate = 180 - (acos(tmp) * 180 / 3.14159);
          turnRateSum += turnRate;

        }
      }
      if (cnt > 1) {
        turnRate = turnRateSum / cnt;
        if (turnRate < bestTurnRate)
          bestTurnRate = turnRate;
      }
    }
  }
  if (bestTurnRate == 10000.0)
    return sqrt(-1);
  return bestTurnRate;
}

void tracker::removeOldTracks(int frameCount, int iSkip, Mat imgL, Mat imgR, Mat imgLc, Mat imgRc, char *baseFileName) {
  vector<vector<PotentialBug> >::iterator ittracks;
  vector<PotentialBug> OneBug;
  PotentialBug pb;
  double trackDisplacement;
  int trackedFrames;
  Mat outImgL, outImgR, outImg;
  double directedness, bestDirectedness, bestLDisplaceV, bestTurnRate;
  ;
  char stringBuffer[100];

  //if (frameCount + iSkip > 10000)
  //  exit(0);
  ittracks = tracks.begin();
  int cnt = 0;
  while (ittracks != tracks.end()) {
    pb = ittracks->back();
    OneBug = (*ittracks);
    if (frameCount - pb.GetLastFrame() > TRACKCLEANOUT) {
      trackDisplacement = cv::norm(OneBug[0].GetLocation() - pb.GetLocation());
      trackedFrames = pb.GetLastFrame() - OneBug[0].GetLastFrame();
      if (trackedFrames == 0) {
        ittracks++;
        continue;
      }


      //printf("Best turn rate was %8.5f \n", bestTurnRate);
      //if (trackDisplacement > CPs1.GetTrackerMinSize() -150 && trackedFrames > 7  && (OneBug.size()/trackedFrames) > 0.5) {
      if (trackDisplacement > 300 && OneBug.size() >= 5) { // && (OneBug.size() / (double) trackedFrames) > 0.25) {
        directedness = calcDirectedness(OneBug);
        bestDirectedness = calcDirectednessStep(OneBug, 5);
        bestLDisplaceV = calcLDisplaceVStep(OneBug, 5);
        bestTurnRate = calcTurnRateStep2(OneBug, 5);
        
        //Set defaults if we can't calculate one of these parameters
        if (isnan(bestTurnRate))
          bestTurnRate = 5.0;
        if (isnan(directedness))
          directedness = 1.5;
        if (isnan(bestDirectedness))
          bestDirectedness = 1.01;
        if (isnan(bestLDisplaceV))
          bestLDisplaceV = 1.5;
        double logistic2 = 57.7776 - 56.3353 * bestDirectedness - 1.53331 * directedness + 2.07898 * OneBug.size() / (double) trackedFrames;        
        double lScore2 = exp(logistic2) / (1 + exp(logistic2));
        double logistic = 1.951 - 0.1053139 * bestTurnRate - 2.3771410 * directedness + 0.0025254 * trackDisplacement;
        double lScore = exp(logistic) / (1 + exp(logistic));
        //printf("Score: %8.5f, %8.5f\n", lScore, lScore2);
        if (lScore > 0.2) {
        //if (lScore > -1) {
          printf("%8.1f,  %7d,  %7d,  %7d, ", trackDisplacement, OneBug[0].GetLastFrame() + iSkip, pb.GetLastFrame() + iSkip, trackedFrames); //, OneBug.size());
          printf("%5.3f, %8.5f, %8.5f, %8.5f, %8.5f, ", OneBug.size() / (double) trackedFrames, directedness, bestDirectedness, bestLDisplaceV, bestTurnRate);
          printf("%5.3f, %5.3f, ", lScore, lScore2);
          printf("%8.2f,  %8.2f, ", OneBug[0].GetLocation().x, OneBug[0].GetLocation().y);
          if (ID == 0)
            printf("L, ");
          else
            printf("R, ");
          
          printf("%6d\n", OneBug[0].GetIndex());

          //printf("Directedness = %f\n", directedness);

          char jpegFileName[FILENAMESIZE];
          vector<int> compression_params;
          compression_params.push_back(CV_IMWRITE_JPEG_QUALITY);
          compression_params.push_back(JPGQUALITY);

          if (ID == 0) {
            imgLc.copyTo(outImgL);
            retraceSteps2(outImgL, OneBug);
            circle(outImgL, OneBug[0].GetLocation(), 20, Scalar(0, 255, 255), 3, 8, 0);
            sprintf(stringBuffer, " Frame %uL", frameCount + iSkip);
            putText(outImgL, stringBuffer, Point2f(100, 100), CV_FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 0, 255), 2, 8, false);
            imgR.copyTo(outImgR);
            resize(outImgL, outImg, Size(imgL.cols / JPGSIZEREDUCTION, imgL.rows / JPGSIZEREDUCTION), 0, 0, INTER_AREA);
            sprintf(jpegFileName, "%s%06dL-%d.jpg", baseFileName, frameCount + iSkip -TRACKCLEANOUT-1 , OneBug[0].GetIndex());
            imwrite(jpegFileName, outImg, compression_params);
          } else {
            imgRc.copyTo(outImgR);
            retraceSteps2(outImgR, OneBug);
            circle(outImgR, OneBug[0].GetLocation(), 20, Scalar(0, 255, 255), 3, 8, 0);
            sprintf(stringBuffer, " Frame %uR", frameCount + iSkip);
            putText(outImgR, stringBuffer, Point2f(100, 100), CV_FONT_HERSHEY_SIMPLEX, 1.0, cv::Scalar(0, 0, 255), 2, 8, false);
            imgL.copyTo(outImgL);
            resize(outImgR, outImg, Size(imgR.cols / JPGSIZEREDUCTION, imgR.rows / JPGSIZEREDUCTION), 0, 0, INTER_AREA);
            sprintf(jpegFileName, "%s%06dR-%d.jpg", baseFileName, frameCount + iSkip -TRACKCLEANOUT-1, OneBug[0].GetIndex() );
            imwrite(jpegFileName, outImg, compression_params);
          }
        }
      }
      OneBug.clear();
      ittracks = tracks.erase(ittracks);
    } else {
      ittracks++;
    }
    cnt++;
  }
}

list<PotentialBug>::iterator tracker::findNearestNeighbor(PotentialBug& pb, double *MinDis2, double current, bool newPoint) {
  double MinDis = 100000000, dis;
  Point2f old;
  //I think there is a problem here.  The method can run without setting it.  Perhaps start by setting it to potentialBugs.end()?)
  list<PotentialBug>::iterator it;
  it = potentialBugs.end();
  current = sqr(current);
  for (list<PotentialBug>::iterator list_iter = potentialBugs.begin();
          list_iter != potentialBugs.end(); list_iter++) {
    old = list_iter->GetLocation();
    dis = (sqr(old.x - pb.GetLocation().x) + sqr(old.y - pb.GetLocation().y));
    if (dis < MinDis && ((dis > (current + 0.00001)) || newPoint)) {
      MinDis = dis;
      it = list_iter;
    }
  }
  *MinDis2 = sqrt(MinDis);
  return it;
}

list<PotentialBug>::iterator tracker::findNearestNeighbor2EndOfTrack(double *MinDis2, int track2Follow) {
  double MinDis = 100000000, dis;
  Point2f old;
  vector<PotentialBug> OneBug;
  PotentialBug pb;
  //I think there is a problem here.  The method can run without setting it.  Perhaps start by setting it to potentialBugs.end()?)
  list<PotentialBug>::iterator it;
  it = potentialBugs.end();

  if (track2Follow >= 0) {
    OneBug = tracks[track2Follow];
    pb = OneBug[OneBug.size() - 1];
  }

  for (list<PotentialBug>::iterator list_iter = potentialBugs.begin();
          list_iter != potentialBugs.end(); list_iter++) {
    if (list_iter->GetIndex() == track2Follow)
      continue;
    old = list_iter->GetLocation();
    dis = (sqr(old.x - pb.GetLocation().x) + sqr(old.y - pb.GetLocation().y));
    // if (dis < MinDis && ((dis > (current + 0.00001)) || newPoint)) {
    if (dis < MinDis) {
      MinDis = dis;
      it = list_iter;
    }
  }
  *MinDis2 = sqrt(MinDis);
  return it;
}

void tracker::removeLastPoint(int track2Follow) {
  tracks[track2Follow].pop_back();
}

void tracker::addPotentialBug(PotentialBug * pb) {

  pb->SetIndex(-1);
  potentialBugs.push_front(*pb);
}

int tracker::getNumPotentialBugs() {
  return potentialBugs.size();
}

void tracker::evaluatePoint(PotentialBug *pb, unsigned int frameCount, int track2Follow) {
  double NNDistance;
  list<PotentialBug>::iterator it, it2;
  PotentialBug replacedBug;
  vector<PotentialBug> OneBug;
  double dist2, dist1, dist3;
  int idx, tmpdex;

  it = findNearestNeighbor(*pb, &NNDistance, 0, true);
  int x = it->GetIndex();
  x = it->GetLocation().x;
  x = it->GetLocation().y;

  if (NNDistance > CPs1.GetTrackerMinSize() || (it == potentialBugs.end())) { //This seems to be a new bug
    pb->SetLineColor(Scalar(randomInt(0, 255), randomInt(0, 255), randomInt(0, 255)));
    pb->SetIndex(nTracks++);
    vector<PotentialBug> newvec;
    newvec.push_back(*pb);
    tracks.push_back(newvec);
    potentialBugs.push_front(*pb);
  } else {
    OneBug = tracks[it->GetIndex()];

    while (OneBug.size() > 1 && (pb->GetLastFrame() == OneBug[OneBug.size() - 1].GetLastFrame())) {
      idx = OneBug.size() - 1;
      dist2 = sqrt(sqr(OneBug[idx].GetLocation().x - OneBug[idx - 1].GetLocation().x) + sqr(OneBug[idx].GetLocation().y - OneBug[idx - 1].GetLocation().y));
      if (NNDistance < dist2) { //we need to swap in pb and continue search for old value
        replacedBug = OneBug[idx];
        unsigned tmp3 = pb->GetIndex();
        OneBug[idx] = *pb;
        *pb = replacedBug;
        it = findNearestNeighbor(*pb, &NNDistance, dist2, false);
      } else //(dist2 < NNDistance))
        it = findNearestNeighbor(*pb, &NNDistance, NNDistance, false);
      if (NNDistance > CPs1.GetTrackerMinSize())
        break;
      OneBug = tracks[it->GetIndex()];
    }

    if (NNDistance > CPs1.GetTrackerMinSize()) { //This seems to be a new bug
      pb->SetLineColor(Scalar(randomInt(0, 255), randomInt(0, 255), randomInt(0, 255)));
      pb->SetIndex(nTracks++);
      vector<PotentialBug> newvec;
      newvec.push_back(*pb);
      tracks.push_back(newvec);
      potentialBugs.push_front(*pb);
    } else {
      if (it->GetIndex() == track2Follow) {
        line(*img, pb->GetLocation(), it->GetLocation(), it->GetLineColor(), 2, 8);
      }
      it->SetLocation(pb->GetLocation());
      it->SetLastFrame(frameCount);
      //if (NNDistance != 0) {
      tracks[it->GetIndex()].push_back(*pb);
      //}
    }
  }

}

void tracker::followATrack(int index) {
  double NNDistance;
  //vector<vector<PotentialBug> >::iterator tracked;
  vector<PotentialBug> OneBug;
  vector<PotentialBug>::iterator iter;
  list<PotentialBug>::iterator it;
  PotentialBug pb;

  OneBug = tracks[index];
  iter = OneBug.end()--;
  int x = iter->GetIndex();
  x = iter->GetLocation().x;
  x = iter->GetLocation().y;
  pb = *iter;
  it = findNearestNeighbor(*iter, &NNDistance, 0, true);
  x = it->GetIndex();
  x = it->GetLocation().x;
  x = it->GetLocation().y;

}

PotentialBug tracker::reportTrackPosition(int index, int track2Follow) {

  vector<PotentialBug> OneBug;
  vector<PotentialBug>::iterator iter;
  list<PotentialBug>::iterator it;
  PotentialBug pb;
  Point2f pos;

  OneBug = tracks[track2Follow];
  pb = OneBug[index];
  return (pb);
}

void tracker::setCPs1(CommonParameters CPs1) {
  this->CPs1 = CPs1;
}

CommonParameters tracker::getCPs1() const {
  return CPs1;
}

void tracker::writeATrack(int index, int mAvr, double fps, char * baseFileName, char *trackname) {
  vector<PotentialBug> OneBug;
  char trackFileName[250];
  FILE *trackerFile;
  trackerFile = fopen(baseFileName, "w");
  printf("Writing track %u\n", index);
  //fprintf(trackerFile, "Mean distance %7.4f, mean degrees %7.4f, fps: %7.3f\n", getTrackAverageDistance(index), getTrackAverageDegreesTurned(index, mAvr), fps);
  fprintf(trackerFile, "Mean distance %7.4f, mean weighted degrees %7.4f, unweighted degrees %7.4f, Total distance %7.4f, AverageOver %d, fps: %7.3f\n", getTrackAverageDistance(index, mAvr), getTrackAverageDegreesTurned(index, mAvr), getTrackAverageUnweighterDegreesTurned(index, mAvr), getTrackDistance(index, mAvr), mAvr, fps);

  OneBug = tracks[index];
  for (int j1 = 0; j1 < OneBug.size(); j1++) {
    fprintf(trackerFile, "%5.2f, %5.2f, %u, %6.3f, %6.3f\n", OneBug[j1].GetLocation().x, OneBug[j1].GetLocation().y, OneBug[j1].GetLastFrame(), getPointDegreesTurned(index, j1, mAvr), getPointSignedDegreesTurned(index, j1, mAvr));
  }
  fclose(trackerFile);
}

void tracker::cleanupPotentialBugs(unsigned int currentFrame) {
  for (list<PotentialBug>::iterator list_iter = potentialBugs.begin();
          list_iter != potentialBugs.end(); list_iter++) {
    if ((currentFrame - list_iter->GetLastFrame()) >= BUGCLEANOUT) {
      list_iter = potentialBugs.erase(list_iter);
    }
  }
}

void tracker::removeBug(list<PotentialBug>::iterator list_iterator) {
  potentialBugs.erase(list_iterator);
}

int tracker::getNumTracks() {
  return tracks.size();
}

unsigned int tracker::getTrackSize(unsigned int j) {
  return tracks[j].size();
}

unsigned int tracker::getInitialTrackFrame(unsigned int j) {
  vector<PotentialBug> OneBug;
  OneBug = tracks[j];
  return OneBug[0].GetLastFrame(); //This is a problem, we don't necessarily store the initial frame
}

unsigned int tracker::getFinalTrackFrame(unsigned int j) {
  vector<PotentialBug> OneBug;
  OneBug = tracks[j];
  return OneBug[OneBug.size() - 1].GetLastFrame();
}

double tracker::getTrackAverageDistance(unsigned int j, int secs) {
  double sumDistance = 0.0;
  nPointsInTrack = 0;
  vector<PotentialBug> OneBug;
  OneBug = tracks[j];
  if (OneBug.size() == 1) return 0.0;

  int initialIndex, hingeIndex;
  double sumx1, sumy1, sumx2, sumy2, meanPos1, meanPos2;
  int counter1, counter2;
  for (int j1 = 1; j1 < OneBug.size() - 1; j1++) {
    if (OneBug[j1].GetLastFrame() - OneBug[0].GetLastFrame() < secs)
      continue;
    initialIndex = hingeIndex = j1;
    while (((OneBug[j1].GetLastFrame() - OneBug[initialIndex].GetLastFrame()) < secs) && (initialIndex > 0)) {
      initialIndex--;
    }
    while (((OneBug[j1].GetLastFrame() - OneBug[hingeIndex].GetLastFrame()) < (secs / 2)) && (hingeIndex > (initialIndex + 1)))
      hingeIndex--;

    sumx1 = sumy1 = counter1 = 0;
    for (int j2 = initialIndex; j2 < hingeIndex; j2++) {
      sumx1 += OneBug[j2].GetLocation().x;
      sumy1 += OneBug[j2].GetLocation().y;
      counter1++;
    }
    if (counter1 > 0) {
      sumx1 = sumx1 / counter1;
      sumy1 = sumy1 / counter1;
    }

    sumx2 = sumy2 = counter2 = 0;
    for (int j2 = hingeIndex; j2 <= j1; j2++) {
      sumx2 += OneBug[j2].GetLocation().x;
      sumy2 += OneBug[j2].GetLocation().y;
      counter2++;
    }
    if (counter2 > 0) {
      sumx2 /= counter2;
      sumy2 /= counter2;
    }

    if (counter1 > 0 && counter2 > 0) {
      sumDistance += sqrt(sqr(sumx1 - sumx2) + sqr(sumy1 - sumy2));
      nPointsInTrack++;
    }

  }
  return (sumDistance / nPointsInTrack); //The factor of 2 puts it into per second
}

double tracker::getTrackDistance(unsigned int j, int mAvr) {
  vector<PotentialBug> OneBug;
  OneBug = tracks[j];
  if (tracks[j].size() > 1)
    //return getTrackDistance(j) / (OneBug[OneBug.size() - 1].GetLastFrame() - OneBug[0].GetLastFrame());
    return getTrackAverageDistance(j, mAvr) * nPointsInTrack / mAvr;
  else
    return 0;
}

double tracker::getTrackUnweightedDegreesTurned(unsigned int j, int mAvr) {
  int firstPoint, hingePoint, movingAverage = mAvr;
  nPointsInTrack = 0;
  firstPoint = movingAverage - 1;
  hingePoint = movingAverage / 2;
  double degreesTurned = 0.0;
  double x, y, z, tmp;
  vector<PotentialBug> OneBug;
  OneBug = tracks[j];
  if (OneBug.size() <= firstPoint) return 0.0;
  for (int j1 = firstPoint; j1 < OneBug.size() - 1; j1++) {
    x = sqrt(sqr(OneBug[j1 - hingePoint].GetLocation().x - OneBug[j1 - firstPoint].GetLocation().x) + sqr(OneBug[j1 - hingePoint].GetLocation().y - OneBug[j1 - firstPoint].GetLocation().y));
    y = sqrt(sqr(OneBug[j1 - hingePoint].GetLocation().x - OneBug[j1].GetLocation().x) + sqr(OneBug[j1 - hingePoint].GetLocation().y - OneBug[j1].GetLocation().y));
    z = sqrt(sqr(OneBug[j1].GetLocation().x - OneBug[j1 - firstPoint].GetLocation().x) + sqr(OneBug[j1].GetLocation().y - OneBug[j1 - firstPoint].GetLocation().y));
    //acos domain is -1 to 1
    nPointsInTrack++;
    if (z == (x + y)) //There was no turn
      continue;
    if (z == abs(x - y)) {//180 degree turn
      degreesTurned += 180;
      continue;
    }
    tmp = (sqr(x) + sqr(y) - sqr(z)) / (2 * x * y);
    if (tmp <= 1.0 && tmp >= -1.0)
      degreesTurned += (180 - (acos(tmp) * 180 / 3.14159));
  }
  return degreesTurned / nPointsInTrack;
}

double tracker::getTrackAverageUnweighterDegreesTurned(unsigned int j, int mAvr) {
  vector<PotentialBug> OneBug;
  OneBug = tracks[j];
  if (tracks[j].size() > 1)
    //return getTrackDegreesTurned(j, mAvr) / (OneBug[OneBug.size() - 1].GetLastFrame() - OneBug[0].GetLastFrame());
    return getTrackUnweightedDegreesTurned(j, mAvr);
  else
    return 0;
}

double tracker::getTrackDegreesTurned(unsigned int j, int mAvr) {
  int firstPoint, hingePoint, movingAverage = mAvr;
  nPointsInTrack = 0;
  firstPoint = movingAverage - 1;
  hingePoint = movingAverage / 2;
  double degreesTurned = 0.0;
  double x, y, z, tmp;
  vector<PotentialBug> OneBug;
  OneBug = tracks[j];
  if (OneBug.size() <= firstPoint) return 0.0;
  for (int j1 = firstPoint; j1 < OneBug.size() - 1; j1++) {
    x = sqrt(sqr(OneBug[j1 - hingePoint].GetLocation().x - OneBug[j1 - firstPoint].GetLocation().x) + sqr(OneBug[j1 - hingePoint].GetLocation().y - OneBug[j1 - firstPoint].GetLocation().y));
    y = sqrt(sqr(OneBug[j1 - hingePoint].GetLocation().x - OneBug[j1].GetLocation().x) + sqr(OneBug[j1 - hingePoint].GetLocation().y - OneBug[j1].GetLocation().y));
    z = sqrt(sqr(OneBug[j1].GetLocation().x - OneBug[j1 - firstPoint].GetLocation().x) + sqr(OneBug[j1].GetLocation().y - OneBug[j1 - firstPoint].GetLocation().y));
    //acos domain is -1 to 1
    nPointsInTrack++;
    if (z == (x + y)) //There was no turn
      continue;
    if (z == abs(x - y)) {//180 degree turn
      degreesTurned += 180 * (x / ((OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame()) +
              y / (OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame())));
      continue;
    }
    tmp = (sqr(x) + sqr(y) - sqr(z)) / (2 * x * y);
    if (tmp <= 1.0 && tmp >= -1.0)
      degreesTurned += (180 - (acos(tmp) * 180 / 3.14159)) * (x / ((OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame()) +
            y / (OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame())));
  }
  return degreesTurned / nPointsInTrack;
}

double tracker::getTrackAverageDegreesTurned(unsigned int j, int mAvr) {
  vector<PotentialBug> OneBug;
  OneBug = tracks[j];
  if (tracks[j].size() > 1)
    //return getTrackDegreesTurned(j, mAvr) / (OneBug[OneBug.size() - 1].GetLastFrame() - OneBug[0].GetLastFrame());
    return getTrackDegreesTurned(j, mAvr);
  else
    return 0;
}

void tracker::repairTrack(int t2f, Point2f p, unsigned int frameCount) {
  vector<PotentialBug> OneBug;
  PotentialBug pb;
  list<PotentialBug>::iterator pb2;
  double NNDistance;
  OneBug = tracks[t2f];
  int x = OneBug[OneBug.size() - 1].GetLastFrame();
  if (x == frameCount) { //We need to pop the last frame - perhaps a while??
    OneBug[OneBug.size() - 1].SetIndex(-1);
    OneBug.pop_back();
  }
  x = OneBug[OneBug.size() - 2].GetLastFrame();
  pb.SetLineColor(OneBug[OneBug.size() - 2].GetLineColor());
  pb.SetIndex(t2f);
  pb.SetLocation(p);
  pb.SetLastFrame(frameCount);
  tracks[t2f].push_back(pb);
  pb2 = findNearestNeighbor(pb, &NNDistance, 0, true);
  x = pb2->GetLocation().x;
  x = pb2->GetLocation().y;
  x = pb2->GetIndex();
  pb2->SetIndex(t2f);
}

int tracker::createNewTrack(Point2f p, int frame) {
  PotentialBug pb;
  vector<PotentialBug> OneBug;
  pb.SetLocation(p);
  pb.SetLastFrame(frame);
  pb.SetLineColor(Scalar(randomInt(0, 255), randomInt(0, 255), randomInt(0, 255)));
  pb.SetIndex(nTracks++);
  vector<PotentialBug> newvec;
  newvec.push_back(pb);
  tracks.push_back(newvec);
  potentialBugs.push_front(pb);
  return (pb.GetIndex());
}

void tracker::convertPBs2Tracks(int frameCount) {
  list<PotentialBug>::iterator list_iter = potentialBugs.begin();
  PotentialBug pb2;
  while (list_iter != potentialBugs.end()) {
    if (!list_iter->IsTracked()) {
      list_iter->SetLineColor(Scalar(randomInt(0, 255), randomInt(0, 255), randomInt(0, 255)));
      list_iter->SetIndex(nTracks++);
      vector<PotentialBug> newvec;
      pb2 = *list_iter;
      newvec.push_back(pb2);
      tracks.push_back(newvec);
    }
    list_iter = potentialBugs.erase(list_iter);
  }
}

int tracker::findNearestEndofTrackToPoint(Point2f p) {
  vector<PotentialBug> OneBug;
  double MinDis = 10000000;
  double distance;
  unsigned int trackIndex;
  if (tracks.size() == 0) {
    return -1;
  }
  for (int j = 0; j < tracks.size(); j++) {
    OneBug = tracks[j];
    //for (int k = 0; k < OneBug.size() - 1; k++) {
    int k = OneBug.size() - 1;
    distance = sqr(OneBug[k].GetLocation().x - p.x) + sqr(OneBug[k].GetLocation().y - p.y);
    if (distance < MinDis) {
      trackIndex = j;
      MinDis = distance;
    }
    //}

  }
  return trackIndex;
}

double tracker::getPointDegreesTurned(unsigned int j, unsigned int j1, int mAvr) {
  int firstPoint, hingePoint, movingAverage = mAvr;
  firstPoint = movingAverage - 1;
  hingePoint = movingAverage / 2;
  double degreesTurned;
  double x, y, z, tmp;
  vector<PotentialBug> OneBug;
  OneBug = tracks[j];
  if (j1 < firstPoint) return 0.0;
  if (OneBug.size() <= firstPoint) return 0.0;
  x = sqrt(sqr(OneBug[j1 - hingePoint].GetLocation().x - OneBug[j1 - firstPoint].GetLocation().x) + sqr(OneBug[j1 - hingePoint].GetLocation().y - OneBug[j1 - firstPoint].GetLocation().y));
  y = sqrt(sqr(OneBug[j1 - hingePoint].GetLocation().x - OneBug[j1].GetLocation().x) + sqr(OneBug[j1 - hingePoint].GetLocation().y - OneBug[j1].GetLocation().y));
  z = sqrt(sqr(OneBug[j1].GetLocation().x - OneBug[j1 - firstPoint].GetLocation().x) + sqr(OneBug[j1].GetLocation().y - OneBug[j1 - firstPoint].GetLocation().y));
  //acos domain is -1 to 1
  if (abs(z - (x + y)) < 0.0001) //There was no turn
    return 0.0;
  ;
  if (z == abs(x - y)) //180 degree turn
    return 180 * (x / (OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame()) +
          y / (OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame()));
  tmp = (sqr(x) + sqr(y) - sqr(z)) / (2 * x * y);
  if (tmp <= 1.0 && tmp >= -1.0)
    degreesTurned = 180 - (acos(tmp) * 180 / 3.14159);
  return degreesTurned * (x / (OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame()) +
          y / (OneBug[j1 - hingePoint].GetLastFrame() - OneBug[j1 - firstPoint].GetLastFrame()));
}

double tracker::getPointSignedDegreesTurned(unsigned int j, unsigned int j1, int mAvr) {
  int firstPoint, hingePoint, movingAverage = mAvr;
  double angle1, angle2, angle;
  firstPoint = movingAverage - 1;
  hingePoint = movingAverage / 2;
  double degreesTurned;
  double x, y, z, tmp;
  vector<PotentialBug> OneBug;
  OneBug = tracks[j];
  //*distance = sqrt((sum1.x - sum0.x) * (sum1.x - sum0.x) + (sum1.y - sum0.y) * (sum1.y - sum0.y));
  if (j1 < firstPoint) return 0.0;
  if (OneBug.size() <= firstPoint) return 0.0;
  angle1 = atan2((OneBug[j1 - hingePoint].GetLocation().y - OneBug[j1 - firstPoint].GetLocation().y), (OneBug[j1 - hingePoint].GetLocation().x - OneBug[j1 - firstPoint].GetLocation().x)) *180 / 3.14159;
  angle2 = atan2((OneBug[j1].GetLocation().y - OneBug[j1 - hingePoint].GetLocation().y), (OneBug[j1].GetLocation().x - OneBug[j1 - hingePoint].GetLocation().x)) *180 / 3.14159;
  x = sqrt(sqr(OneBug[j1 - hingePoint].GetLocation().x - OneBug[j1 - firstPoint].GetLocation().x) + sqr(OneBug[j1 - hingePoint].GetLocation().y - OneBug[j1 - firstPoint].GetLocation().y));
  y = sqrt(sqr(OneBug[j1 - hingePoint].GetLocation().x - OneBug[j1].GetLocation().x) + sqr(OneBug[j1 - hingePoint].GetLocation().y - OneBug[j1].GetLocation().y));
  angle = angle2 - angle1;
  if (abs(angle) < 0.000001)
    return 0;
  while (angle < -180)
    angle += 360;
  while (angle > 180)
    angle -= 360;
  return angle;
}



