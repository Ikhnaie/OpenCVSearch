/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GlobalDefinitions.h
 * Author: mcaprio3
 *
 * Created on April 7, 2016, 2:14 PM
 */

#ifndef GLOBALDEFINITIONS_H
#define GLOBALDEFINITIONS_H


#define SLOWMOTIONFACTOR 1   //msec delay between frames for analysis - must be 1 or greater
#define FILENAMESIZE 250

#define FILTER_THRESHOLD 4  //was 20 for bedbugs //20
#define FILTER_THRESHOLD3CH 15  //was 3000 for bedbugs, 150 for flies //50
#define FILTER_THRESHOLD3CH_SEP 5  //If this is set to other than zero, all chanels must be less to make black// 40
#define USECOLORFILTER true

#define BUGCLEANOUT 0
#define TRACKCLEANOUT 3
#define JPGSIZEREDUCTION 2
#define JPGQUALITY  65

#define REMAPPING  true //requires calibration

#define JORGE false


#define HYBRIDADAPTIVEBACKGROUNDMODELING false  //Does not currently use remapping
#define HYBRIDMAXINITIALRUN  1800


#define FEATUREMATCH false


#endif /* GLOBALDEFINITIONS_H */

