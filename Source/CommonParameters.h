/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   CommonParameters.h
 * Author: Mike
 *
 * Created on September 28, 2016, 7:55 PM
 */

#ifndef COMMONPARAMETERS_H
#define COMMONPARAMETERS_H

#include <stdlib.h>
#include <stdio.h>
#include "opencv2/opencv.hpp"
#include "opencv2/features2d/features2d.hpp"

using namespace cv;
using namespace std;

class CommonParameters {
public:
    Ptr<BackgroundSubtractorMOG2> bg, bg2; 
     SimpleBlobDetector::Params params;   
    CommonParameters();
    CommonParameters(const CommonParameters& orig);
    virtual ~CommonParameters();
    void SetGaussianBlurSize(int GaussianBlurSize);
    int GetGaussianBlurSize() const;
    void SetTrackerMinSize(int TrackerMinSize);
    int GetTrackerMinSize() const;
    void SetAdaptiveBackgroundModeling(bool AdaptiveBackgroundModeling);
    bool IsAdaptiveBackgroundModeling() const;
    void SetGaussian(bool Gaussian);
    bool IsGaussian() const;
    void SetParams(SimpleBlobDetector::Params params);
    SimpleBlobDetector::Params GetParams() const;
    void SetDilate1(int Dilate1);
    int GetDilate1() const;
    void SetErode2(int Erode2);
    int GetErode2() const;
    void SetErode1(int Erode1);
    int GetErode1() const;
    void SetKalmanCircleSize(int KalmanCircleSize);
    int GetKalmanCircleSize() const;
    void SetTrackCircleSize(int TrackCircleSize);
    int GetTrackCircleSize() const;
    void SetKeyPointCircleSize(int KeyPointCircleSize);
    int GetKeyPointCircleSize() const;
private:
    bool Gaussian, AdaptiveBackgroundModeling;
    int TrackerMinSize;
    int GaussianBlurSize;   
    int Erode1, Erode2, Dilate1;
    int KeyPointCircleSize, TrackCircleSize, KalmanCircleSize;

};

#endif /* COMMONPARAMETERS_H */

